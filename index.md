---
layout: home
---

![]({{site.baseurl}}/images/181120_Visuals02.gif)

&nbsp;

<div class="container">

This website is dedicated to my journey through the Master in Emergent Futures at the Institute for Advanced Architecture in cooperation with Elisava School of Design and Engineering, Barcelona. Within this journal I am keeping record of all my projects, experiences and my master thesis.

During our master we learn tools and methodologies to prepare us to become agents of change in multiple professional environments. Each student is focusing on the design of interventions in their field of interest. May it be the futures of agriculture, politics or artificial intelligence.

</div>
