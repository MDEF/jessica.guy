---
layout: page
permalink: /about/
---

![]({{site.baseurl}}/images/AboutmePicture.jpg)

## Hi there! My name is Jessica Guy

//CURRENT
There is no real current or now. Because when you are reading this, this will already be the past. And probably I already have something new in which I am interested in. Nevertheless I think it is valuable to know what I am currently interested in, especially for the future-me. I know I like being in my comfort zone. Being in nature or in the workshop, working with wood and other materials. But where would I be, if I would always stick to the things I know? Defiantly not in Barcelona. I have always been a curious mind, exploring everything around me. Through that I have always found new fields of interest. Currently I am seeking for interventions and context. Trying to be a part of a network of people designing for emergent futures.

I am also trying my luck with Aeroponics with Emily Whyman. Interested in how this works out? Check out our [Aeroponics Project Diary](https://mdef.gitlab.io/jessica.guy/aeroponics/).

//FUTURE
What will the future bring? This is probably one of the most asked questions, besides of ‘what is the meaning of life’. I like to think that I live everyday after another, not trying to make any plans. But the reality is different. I am always worrying if I will ever find a job which will suit me? And if this job will fulfil my dreams of doing something meaningful. But I am trying to see it a bit more optimistic. I will just have to find the right people at some point. With them I would like to work in a community to tackle occurring problems. Until then I will take the rocky road.
