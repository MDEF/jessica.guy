---
layout: page
title: Workshops
permalink: /workshops/
---

![]({{site.baseurl}}/images/F_Photo01.gif)

### Communities

The internet is seen as an opportunity to expand and enhance community behaviour. Knowledge and information can be exchanged within seconds. Through a click anyone can be connected to a like-minded person. And it gets easier every day. But there are setbacks in this development. Lack of online friction and an increase of anonymity are some of the few results. We don't know anything about our next-door neighbours, nor anything about the person who is cyberbullying. We often feel detached and alone in the world, in a time in which everything is connected. How can we counteract this evolution?

### Sharing offline time

The lack of community engagement is a huge problem, and cannot be solved by one person alone. Let's disconnect for a few hours and spend some offline time in your neighbourhood. Almost every city offers yoga or language classes for free. [Meetup](https://www.meetup.com/) and facebook groups update on local activities. We can raise community engagement, together. It doesn't have to be that 'formal' share time and knowledge with your friends and friends of friends. With the goal to build strong bonds throughout the city, we can extend the network. And my personal way of doing that is through workshops.

### Food Workshops

In the recent years I've became very aware of the problems in the food industry. I changed my personal life style and share information about how to grow, produce, store and ferment your own food. Everyone has something to share, at least a little time to participate in the community.

My workshop is dedicated to anyone who enjoys food and like to exchange thoughts about food. The classes contain the basics of fermentation and is inspired by the [Zero Waste Chef](https://zerowastechef.com/). It's all about tangible experience of making your own food.

![]({{site.baseurl}}/images/F_Photo02.gif)

![]({{site.baseurl}}/images/F_Photo03.gif)
