---
layout: page
permalink: /aeroponics/
---

![]({{site.baseurl}}/images/lettuce.jpg)


### Farming today

There is this delusion that we don't have enough food for the growing population of our earth. We are constantly utilising more and more landscapes converting it into farmland. The problem lays somewhere else or lets say it is way more eclectic. We are able to produce more than enough calories to feed the whole world. We are just not able to distribute it equally. The global consumer behaviour has to change drastically. It is time to adjust not only the way how we consume but also how we cultivate.

The competition in agriculture is high. The words 'go big or die' are used ever so often within the realms of the farmers. Small farms, doesn't matter if they are organic or not, are having a hard time. This downward spiral is forcing farms to close down all over the world.

### How can the future of farming look like?

The current traditional system is not working anymore. The gap between big cooperations and small farmers are too high. But what can we do to approach this problem? The consumer appetite for local grown, organic, craft and zero waste has grown. These niche markets have a potential which can go hand in hand with a more sustainable live style. In the desirable future of a circular economy, goods and food can be produced locally. How can we decrease shipping, transportation, use of pesticides, CO2 and enable realtime consume of crop?

### One possible future

My friend [Emily](https://mdef.gitlab.io/emily.whyman/) and I are currently working on one of the possible 'farming of tomorrow' scenarios. We are building a growing community in cooperation with Rasmus from [NextFood](https://nextfood.co) based on the growstack aeroponics system.

![]({{site.baseurl}}/images/radish.jpg)

### What is Aeropnics?

Aeroponic systems have been proposed as a functional, sustainable solution for urban agriculture. There are many positive arguments for the use of aeroponics, ranging from being environmentally efficient to to spatially adequate. Aeroponics uses minimal amounts of water and grows plants in a controlled environment - meaning the system can be manipulated to encourage the most productive plant growth. The controlled environment also highly reduces the chance of disease or pest. The system is monitored via a software (Grafana) which provides real-time information. The future potential for an aeroponics system within university infrastructure is enormous - it can easily integrate into a students routine (as it has with ours) through routine analysis, maintenance and harvesting. Not only can students learn about food cycles, but also about the technology, electronics and system interface.

Aeroponics is a new technology - also new for us - which we are learning through tangible experience. Being able to grow food from seed to harvest has enabled a deeper understanding and connection to how we produce, sell and buy food within cities, eventually leading to this application. We want to share these experiences and lessons we have learnt through the aeroponics system and encourage implementation of these in every school and university. The beauty of these systems is there malleability to location - they can be downsized or upsized depending on demand and fit snugly into spare spaces which many are often unaware exist.

![]({{site.baseurl}}/images/full.jpg)
