---
title: Fabacademy
period: 30 January 2019
date: 2019-01-30 12:00:00
term: 1
img: images/Fabacademy-Cover.png
published: true
layout: fabacademy
---


Ever wondered how things are made? What a 3D printer can do? And what a CNC is? All these topics are covered during the weekly classes of Fab Academy, and anyone can participate. Students basically learn "how to make almost anything". We are taking part in this distributed educational model in course of the Master for Emergent Futures.

This weeks task is to set up a fab academy section within out existing website as well as getting to know the basic project management tools. I decided to add the fabacademy to my journeys, as I don't separate my website into the three semesters. I am differentiating the classes from last and this semester only visually by their covers within the journal.

![]({{site.baseurl}}/images/Websitechange.png)

![]({{site.baseurl}}/images/Git01.png)

![]({{site.baseurl}}/images/Git02.png)


### Understanding GIT

We already had the opportunity to get to know [GIT](https://git-scm.com/) during the first week of our master. Git is a distributed version control system. It is relatively easy to learn and keeps a record of all changes. Being able to code html, css and more you definitely need time and practice. I am currently programming a second website to get to know all the commands and layout options. Until that website is set up I will upload all my content here.

My website runs with [Jekyll](https://jekyllrb.com/), a static website generator. The advantage of using Jekyll is that I can run my Website locally, and with that I am able to see the changes in realtime. For all my basic commands I use [Hyper](https://hyper.is/), which is an electron based terminal. The text is written in [Typora](https://typora.io/), a markdown editor. Everything comes together in [Atom](https://atom.io/). Atom basically displays the website structure. Within Atom all changes to the content and layout can be made, and pushed. This workflow makes it easy for me to upload my content easy and fluid.

### Project Decription

I will focus on my master project for my Fab Academy weekly assignments. I am exploring how I could hack the human interface.

In the past years human-nature-technology interaction has been drastically influenced by technological revolution. Analog human-interaction is being teared apart by constant digital updates, self-optimisation, real-time self-editing and is shifting our focus on quantity rather than quality.

The main idea is to redesign our interaction and habits with technological devices. After observing our habits and rituals, I would like to intervene in the moment in which we reach out to our phones.

The goal is to encourage mono-tasking, time for self and cultural reoralisation, through learning how to be alone and how to be an active listener. As a result we could be able to learn and cherish digital time and finding pleasure in analog time. Creating active prosumers - instead of passive consumers.

For that I would like to create an artefact or object, close to our body, which interacts with us when using technology, as the first instance of change.
