---
title: Emergent Business Models
period: 30 January 2019
date: 2019-05-05 12:00:00
term: 1
img: images/Emergentbusinessmodels.png
published: true
---

### Class content

![]({{site.baseurl}}/images/EBM-02.svg)

![]({{site.baseurl}}/images/EBM-03.svg)

![]({{site.baseurl}}/images/EBM-04.svg)

![]({{site.baseurl}}/images/EBM_Newspaper.svg)

### Reflection

Personally I believe that the class of *Emergent Business Models* was of great value. We have learned to find ways how to make our project more realistic. Although I have to say that many of us might not have been ready for this, me included. We all have been struggling the past weeks to have time to get our assignments done and also work on the our intervention. At times this class felt like being to early, and other it seemed like it has been to late. Overall I think that this class has helped me a lot. We have learned to which Innovation have been triggering or making our interventions possible. But not on a superficial level. Javier Creus and Valeria Righi both made us think and unravel passed events and how they have been influencing our world today. I wished I would have had more time to deeper develop my business model, but I hope I will have time for this in the upcoming years.
