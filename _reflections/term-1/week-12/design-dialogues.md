---
title: Design Dialogues
period: 23 December 2018
date: 2018-12-23 12:00:00
term: 1
img: images/Dialogues-Cover.png
published: true
---

Nature is able to create 8.5 million species. Now we have the chance to create a new species through AI. But instead of creating something we just replicate ourselves. Why don't we use this chance to create something genuinely new?  Based on this idea I want to find new ways how we can communicate with and through technology using natures methodologies. The AI or technological artefact could be based on a new set of skills, learning from natures relationships, hacking the human interface to create a new way of communication. The goal is to improve the nature-human-technology communication.

The presentation shows my journey through the past three months at IAAC. Every step resembles a tool/ skill/ information which I have gained through the weeks. Every step is shown in a different Interface. If I am able to find ten different interfaces within two days, imagine what an AI could find/ create within a week. How can the interface of the future look like if it based on a natural model like a plant or corals?

![]({{site.baseurl}}/DDPhoto03.jpg)


### Opportunities
Which possible outcomes and connection could be formed through this project?
: Combining technology and nature
: Creating a 'tech' language based on something else than words and 1/0
: Opening the possibility that technology could be an open space for exploration
: Exploring new rituals with/ in technology
: Creating a space to learn how to interact with AI/ technology
: New 'typology' for computer/ technology
: Designing an experience rather than a 'thing'
: Designing a sense which we don't have
: Finding ways of knowing the unknown


### Key questions
Questions which I was asked during the presentation
: How can AI/ technology communicate with us or how we can communicate through AI/ technology?
: How will I train the AI or cognitive object?
: Why does everything has to be more 'easy'? Learning a language or an instrument takes time. The time spent on learning something new is considered precious. Can learning how to communicate with and through AI similar?
: How can we give time to build a relationship?
: What can I make happen in the next six months?
: What is my area of intervention?
: How can we make AI/ Technology understandable?
: What is intelligence?
: Can it be intelligible? (Cognitive rather than AI)


### Things to dig into
- Look up new trends and possibilities
- Biosemiotics
- Try out as much as possible, making it physical/ tactile etc.
- Make an assumption and try it out (realtime feedback loop)
- Explore shared spaces of trust and understanding
- History of language
- Low resolution Interfaces and slow technology
- 'Slow art' using technology
- 'What do you mean' synonyms
- Hacking human interfaces
- Finding new methodologies to play with
- Information theory/ entropy
- Collaborative intelligence and emotional intelligence


### Difficulties
- After opening up and dipping in the sea of chaos and possibilities I have to convert and narrow down what I actually want to do
- Finding a field of application/ intervention
- Consider that everyone has a different point of view
- Finding balance between technology with 'friction' and 'no friction'
- Making tech which doesn't overwhelm us with information
- Creating a space for discovery, like learning a new craft - giving it friction  
- Using the time which we have efficient
- Having a more critical point of view
- Braking it down to the essence of its purpose
- Finding the sweet sport between interaction and ideas


### What to do now
For the next two - three weeks I will make a thought experiment. I will keep a diary, writing down every moment and idea in which I think and intelligent/ cognitive 'thing' could help me. I will try to be very open and creative and won't 'kill' any ideas in this first step.

After that I will try to sort the ideas, choosing the ones with the most potential. I can check the ideas for a 'right  to exist' in imagined use case scenarios.

I will explore different methodologies with which I can play with to be more creative. For example for every idea I could have a utilitarian, magic and radical version. Playing with different ranges between digital and analog.
