---
title: Design for 2050
period: 30 January 2019
date: 2019-04-30 12:00:00
term: 1
img: images/Designfor2050.png
published: true
---

<iframe src="https://player.vimeo.com/video/341238982" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


### A Friday in 2050

Waking up in the morning, getting up and deciding on an artefact which you use to connect to different systems, trees, mycelium, materials, tech etc. Using one will decide on the frequency on which you travel, explore and connect with. We configuring ourself with these totem deciding on which network we want to be part with. Living outside of the network is still possible but  through the artefact we are able to see connections and interact with non human species. The frequencies bandwidths are fluid at times but generally predetermined. Work is something different today, we work on everything collectively, we work together with different species. And everyday you can do something else seeing online on what stare which projects are. This way everyone can participate in any projects they find relevant. The work is tagged, therefore I can search for my work on my daily interests and what I want to learn. Today I don't know what I want to do, or if I feel like work at all. Therefore i am using the randomiser which has been invented by Neil in 2010, 40 years ago. The selected topic does not sound interesting, therefore I go outside reflecting on my thoughts and feelings first. To rethink about what my todays goal is going to be and where I want to put my time in. I stroll around the cityscape. Today I am connected with materials. The surfaces and materials are shifting and my glare is focused on them. I can see the connections between touches, feelings and materials. Therefor I decide to be a material designer today with a cyborg friend of mine.


### Reflection
The last term gave my project and my personal development another push in the right direction. The class "Design for 2050" with Andres from IAM, was more than inspiring. We have learned that working together as a group gives the not only velocity but also needed comfort. We have started the class with a check-in and ended every class with a check-out. This helped to start the class together, with everyone stating how they feel and what they expect from that day. And ending with stating how the class went and a short reflection. Which is important to know, because then you can be prepared for when talking or working together for this session. Andres was made clear that he in this class is not a teacher, but part of the group. This gave us the feeling of being appreciated and taking serious because we are all on the same level and every opinion counted.

We needed a step in and out of reality in times in which we all have our heads either in the clouds or deep down in our projects. The classes with Andres were able to provide us exactly with that. Even though we have been working very speculative on ideas for our own Black Mirror episodes, which seemed in the first moment a little awkward, it helped us in finding our own personal ways of speculating about our projects within the master. All the work we had to put into the classes were managed in the time we had during the class. He made it clear that he does not want to add extra work but still wants us to work hard, together. One was able to tell that everyone enjoyed the classes with him because (even though this is kind of sad) in every class everyone showed up and gave 100%. This makes clear that his teaching methods are more than good.


Andres helped us to to find a nice balance between playful speculation and working on a serious project development. We are all very grateful to have had him as part of our group.
