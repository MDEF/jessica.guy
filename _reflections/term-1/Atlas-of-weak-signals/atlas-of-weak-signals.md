---
title: Atlas of Weak Signals
period: 30 January 2019
date: 2019-01-01 12:00:00
term: 1
img: images/Atlas-Cover.png
published: true
---

## Creating an Atlas
The goal of this class is to create an Atlas of Weak Signals and create scenarios around the emerging topics. In trend research there are tendencies which in foresight can be distinguished as a trend. "Wild cards" are unpredictable and weak signals which can be spotted, sometimes turn out to be a trend and sometimes just vanish. During this class we discuss topics like capitalism, anthropocene, identity and politics. Within these topics there are weak signals. We are mapping the topics out, finding connections and intersections, which are leading to an interactive and growing map of weak signals. This Atlas can be used by us and the new Students of the Master for Emergent Futures to position our projects.

Following are some notes from the classes and the speculative projects we have made during that week.

## Surveillance capitalism
There are multiple crisis with which we are confronted today. What is masculinity? How is life in the times of surveillance capitalism? How does Artificial Intelligence changes our perception? The internet started with the utopian idea of being non hierarchical, private, transparent and free. This myth its fake promises is being shacked. Today the Internet is monopolised and capitalised for the sake of earning more and more money. It has changes and formed our interaction, the online self is detached from our analog "real" self. Almost all of our social interactions happen in the digital space. Friendships and self-value are being measured in "likes" and the quantity of followers. The Identity of the self has been shattered at its roots. The Attention Economy flourishes in the age of the smartphone. Our daily lives are being recorded and the data is being collected for the sake of making more life absorbing services. The constant self reinforcement loops are engaging the people to spend more and more time online/ on their phone. The more you post, the more feedback you get. The reflex to check the phone as soon as one sees a notification is difficult to suppress. Engagement is the keyword of the attention economy, just as clickbait design (The syntax which will make you click on a link). Who you are online and offline are two different persons. How will this change in the future and what can we do about this?

Fun Fact: i = tcp/ip + http → The equation of the internet

### Weak Signals
- Attention protection
- dismantling filter bubbles
- A circular data economy
- The truth wars (Fake news)
- Redesigning social media
- Manipulation

### The Data Burial Service (A circular data economy)
Our group focused on a scenario around the topic "the death of data". We have researched how Russia, China, Europe, and the United States handle data privacy and storage. In a circular data economy, data should be part of the metabolic process. The requirement is that data is owned by the one who produces it. Taking into account that the data storage has an environmental impact it seams only logical that we should give the possibility to data to "die". In our scenario the "Data Burial Service" provides data owners (which everyone is) the service to decide what to do with their data. In certain decision points you have the possibility so sell, donate or erase your data. For example donating your health data for research.

Link to the [Presentation]({{site.baseurl}}/Data-Burial-Service.pdf.pdf)  


## Designing for the Anthropocene
Twelve thousand years ago we started having a physical impact on our planet through settling. And when we stoped being nomadic, we started to fuck things up. Agriculture, architecture, infrastructure etc. all these things have an actual physical impact. We started breeding animals for our own needs and years of stabile development got out of balance. The industrial revolution 150 years ago brought polluted air which makes todays air partially unbreathable. Our influence on our atmosphere is related to labour, which is related to growth, when ultimately is process. The date which we have chosen for our start for the anthropocene (entering a new geological time) is July 16th 1945. When the first atom bomb was set off. The Paris Agreement is a global arrangement between 175 parties with one goal: The deceleration of climate change and ultimately a stable and clean environment on earth for all its inhabitants. We have 1.1 degrees above climate temperature since 1950. How are we going reach the goal to not rise above 1.5/2 degrees? The Paris agreement says that we have to lower emissions drastically. This would mean no more burning of gas, oil or big amounts of wood. Also we have to extract more CO2 out of the atmosphere. Therefor adding biomass through reforestation for example in the Amazon. plastic pollution is so high that we have two massive garbage patches in the ocean. Micro-plastic pollution is already everywhere. The two commodities of the earth is SAND and WATER. Without these we are are not able to survive. The next 11 year are the key years of making a change. This is not a problem we will be able to solve, but we have to adapt to the situation.

Some shocking facts:
- 63 percent of the total CO2 emissions have been emitted into our air in the last 25 years. And we are much worse than we think.
- Burning planet: 411.51ppm And we must avoid a level of 450 parts per million for a chance to keep global warming below 2°C
- 800 million people are living in coastal areas, which will be effected by the melting ice rising the sea levels. The arctic contains a huge amount of methane, these will released as soon as they melt.
- In the same 50 years of acceleration 60% of animal populations since then. We are loosing 20% of insects per decade. The background level of extinctions is around 3 - 5% per decade.

### Week Signals
- Longtermism
- Fighting anthropocene conflicts (movement of species)
- Carbon neutral lifestyles
- Interspecies Solidarity
- Climate Consciousness

### Climate Consciousness
The change of our habits are not changing fast enough, although we have all these fact about climate change. We still consume more than we can produce/ need. We use single use plastic although it is already polluting our oceans, and we are eating through fish. How could we make the change more present? And the connection between the impact on our environment and our actions more noticeable? In our group we thought of real time sensuous feedback loops, to be able to feel from the future the consequence of your actions. Will awareness of the changing climate be taken with more seriousness if we heard and felt messages from the future through our bodies?

Link to the [Presentation]({{site.baseurl}}/Climate-Consciousness.pdf)

## Future of Jobs
Jobs have changed over the years, when ever there is a new technology introduced jobs got replaced or changed. For example labour intense manufacturing processes got easier through automation. Robotic arms can lift heavy parts in car factory, this does not "take away" jobs, but makes life easier for humans. And through the new technologies new jobs open up. The need for experts on how to program and maintain these robots are high. Back then we where not able to see this kind of jobs, because we did not know of the possibilities which will come. The same will happen in the future. We don't know yes what new jobs will appear. Another big fear of society is that when "all" jobs are being taking over by "robots" there will be nothing left to do for humanity. First of all, when we are talking about AI "taking over" our jobs we talk about single task jobs. AI is not yet able to do creative jobs completely nor anything else which need human factors like empathy. But it can assist. And also even if everything will be "taken over", this could mean that manufacturing can optimised. Production of products could be local and cheap and there would be no need of exploiting humans. We could concentrate on ourselves, nature, creativity and communities. It could bring a new age of how we see work and creativity.

### Week Signals
- Future Jobs
- Human-machine creative collaborations
- Fighting AI Bias
- Tech for equality
- Making UBI work

### Sonar 2050 (Human-machine creative collaborations)
For the futures of jobs weeks we have looked into the human-machine creative collaboration in the music business. If we imagine the Sonar festival of 2050, how would this look like? How will the interfaces with which we create music change? Can anyone make music or will artificial intelligence (AI) take over our creative industry? How will the interaction between the musician and the crowd at the concert change? We have concentrated on how the interface of the software can be improved in the future. The AI will make it possible to speed up processes. We can already see today that people using machine learning are profiting from the machines. In the AI can not only show an extensive amount of melody variants but also could take into account how the crowd interacts with the music during a crowd. That mean if everything is interconnected the AI could change the outcome of the music based on the attending spectators. This would mean that every concert is one of a kind. We also prepared a sample of how we can already involve machine learning software into music making today.

Link to the [Presentation]({{site.baseurl}}/human-machine-creative-collaboration.pdf)

## The end of the Nation State

### Weak Signals
- Making world governance
- Rural futures
- Pick your own passport
- Refugee tech

## Identity

### Week Signals
- Non heteropatriarchal innovation
- Imagining Futures that are non western centric
- Reconfigure your body
- Gender Fluidity
- Disrupt ageism
- Metadesign to reclaim the technosphere

## Merging the weak signals - The Data Theft Alliance
For the final presentation of both classes we got into three main groups. My group consisted of [Oliver](https://mdef.gitlab.io/oliver.juggins/), [Gabor](https://mdef.gitlab.io/gabor.mandoki/), [Silvia](https://mdef.gitlab.io/silvia.ferrari/), [Veronica](https://mdef.gitlab.io/veronica.tran/), [Rutvij](https://mdef.gitlab.io/rutvij.pathak/), [Julia](https://mdef.gitlab.io/julia.danae/), [Katherine](https://mdef.gitlab.io/katherine.vegas/) and me. We have created the Data Theft Alliance.
We anticipate that in our future scenario the anticipatory design is the new normal, your life is now decided by corporates, there will be algorithms who own all of your data and what you buy/ where you live and who you talk to will be predetermined. The elections will be coming and the people will feel lost. The corporate state will offer an alternative to the chaos and scariness of the current social, political and economic climate. This scenarios sounds familiar? It is not far from what we can feel today.

Link to the [Presentation]({{site.baseurl}}/Data-Theft-Alliance.pdf)

## Reflection

The two classes in general had a good combination between exploring and finding methodologies. The pace through the weekly assignment gave a certain push to get things done. Researching topics, organising information, making a weekly speculative project and then evaluating it, gave me a structure which I can also apply on my personal project. I think the combination of Jose, Mariana, Ramon and Lucas gives multiple perspectives to each topic. This diversity of opinions is a good representation to "real world jobs/ coworkers/ bosses" and the future in which we will live. Interesting projects and ideas developed especially when we got to point where we where not one opinion. Every week I had the opportunity to be part of a different group, which I really enjoyed. The general structure of the class which was an introduction to the topics, and then gathering in interest groups and discussing project idea with the tutors was productive. For the future I would hope that this class will get more importance. Sometimes I myself feel like I am in a filter bubble. During the five weeks we have opened so many doors which are worth exploring. I think if we would have just a little more time the atlas itself and the resources for it would have been more in depth. We only scratched surface of what is possible.

The tools we have learned during the course of the class are very valuable for me. One on of my classmates introduced us to [Kumu](https://kumu.io/) which is a tool to organise complex data and makes relationship maps. I used Kumu to map out my [Intervention](https://mdef.gitlab.io/jessica.guy/master-project/), related weak signals and other interesting topics. When mapping I found that each one of the branches could be expanded and found relations to other weak signals which I have not seen before. Just as Lucas said, the steps from one to another always have to be clear. And I guess I could go on forever with mapping related topics and intersecting them. It makes the complexity of this world visible. Which brings me back to my master project. All these intersection and invisible connections are something we don't see, which was the topic of the *Climate Consciousness* signal. Climate Consciousness was the signal I could mostly relate to, it hit my project directly on the sweet spot (During the class with Lucas we have renamed it to *Climate Awareness*. Although I have to say that awareness is a term with a predetermined and difficult notion just as consciousness). We have an irreversible impact on our environment and that is not negotiable. The amount of waste in our oceans is so high that you can see the garbage patch on satellite images. The amount of livestock remains will be noticeable in the future soil just as we see today the remains of the antique. Every little thing you do on this earth has an impact, even if not seen directly. And that is something we have to become more aware off. And I want to tackle this problem within the city scale during my master and future. We are interconnected with our environment and other entities.

*Attention Protection* and *A Circular Data Economy* both have had valuable impact on my projects. The research during the week of surveillance capitalism has influenced my way of looking at data privacy, transparency and how data is handle in the city scale. Attention protections was not directly connected to my project before, but research has shown me that also this should be taking into account. How the media and the cooperations are direct our gaze and attention is criminal, and most of us are helpless victims of that. We have reclaim our own will and attention. For a future in which we can move freely and act consciously. *Human-machine creative collaborations* was the last speculative project we had as a group. It was not directly related to my project, because we laid it out in a sense of artificial intelligence enhancing creativity in music. The research during this weak had influence for me. We have discussed how technology can synchronise with our human interaction. In my opinion this balanced synergy between technology - nature and humans will thrive and develop in the next years. Other Interesting topics which relate in a more broader sense to my project were: *Tech for equality*, *Metadesign to reclaim the technosphere* and *Reconfigure your body*. Unfortunately we did not have time to work on these projects in groups. But I will look into them during the upcoming weeks.

Generally I was able to connect my project to other projects of my classmates through this class. Even though Silvia, Veronica and I have different ways of materialising our idea, overall we can relate a lot to each other. We where able to make our own semiotic map based on our project ideas during the classes. The atlas and the scenarios we have created makes it possible to see the world through the lens of the "other" and I think that is one of the most powerful tools I have gained through this class. Our complete Atlas can be seen [here](https://kumu.io/sraza/materials6#aowsscraped).
