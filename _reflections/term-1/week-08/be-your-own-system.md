---
title: Living with Ideas
period: 25 November 2018
date: 2018-11-19 12:00:00
term: 1
img: images/Ideas-cover.png
published: true
---

### What is Speculative Design?

How can you design something for the future without knowing what the future will bring? One way of designing for emergent futures is to speculate how the future will be. It helps us to *form ideas and desires*. By creating a scenario in which an object could be introduced in, we can observe human - object interaction. We can use the outcomes to improve or change the object. The scenario created can be an alternative future based on a different past than ours. It can contain the wildest ideas, no limits are set. It is possible to first create an object and then a future story in which it will 'live'. Or when the scenario is predetermined, we can imagine and create an object which can be set in that scenery. It is a *playful approach* which leaves space for opinions and interpretation. The point of this exercise it to see that a product, no matter what it is, has an *impact beyond the user*. It is part of a whole ecosystem. It influences the society and the environment just as the environment and society influences the object.

This week we created a speculative object ourselves. We all brought everyday objects from home. We imagined a future scenario and a 'what if' question based on that.

### Future 1.2

The human evolved based on our seeing sense. That why we are walking up straight, to have a good view. All our senses are placed and formed the way they are for a certain reason. The positioning of our eyes and ears. Our sense of touch in the finger tips. Everything has its own propose. But what if we hadn't any ears? What if we weren't able to hear sound? In my alternative future we would have a different perception of sound. Based on the desire to enhance our senses we would have modified our body with electronic devices. We would be able to hear with a touch. With the body-modification you could consciously listen to materials.

![]({{site.baseurl}}/181120_LivingwithIdeas_Photo1.gif)

What could these materials sound like? Is it rough, soft, loud, electric or subtle? Would the materials sound the same for every person? Because everyone is perceiving colours differently, I can imagine that it would be the same for hearing materials. Interesting to see is how my behaviour changed using this device. I used my left hand with more caution, to not listen to materials which I am not interested in. While typing on my computer I tried to use my right hand as much as possible. If the device is directly connected to our brain would it be sound or would it rather be vibration? [Synesthesia](https://www.psychologytoday.com/intl/basics/synesthesia) is the ability for example to hear colours or to see sounds. This seems very magical but there are actually people who are capable of that. It is a neurological condition where the stimulation of one sense is also activating another sense. My artefact could be a modification of your body which makes this possible for anyone.

### First Person Perspective

Another way of investigating an idea, is to create a scenario in which you can experience a first person perspective. Although you cannot directly design an experience you can design a framework. In that framework anyone can experience the design with their own point of view. When one designs an object is it not only about designing a product. One also has to consider its impact on its environment and how the interaction with it will be. In the first step it is convenient to try the product/ idea yourself. During a time period the designer can explore the interaction with the object. The product can then be improved based on the experiences and knowledge gained. For example on way of experiencing digital fabric is to use the green-screen technology. Different utilisation scenarios are formed after using the various mediums and digital projections. Where could I see this used? I guess it could be interesting to show emotions through it. For example if you are open for talking or more shy and need to be approached differently. Although this means to show very intimate information about you. Another idea is, to use it in a museum or information scenario. Using it to 'zoom' into information or stories. Maybe showing the history or heritage of objects. The goal of this technique is to start the design from the experience, instead of the product.

![]({{site.baseurl}}/LWIPhoto04.jpg)

### Materializing an Abstract Idea

To experience an idea it is always helpful to make it physical. There is always a way to materialize the idea in some sort. For web-design mockups are used to try out different layouts and connections between pages. In product design almost all objects are tried out at least once, to understand the dimensions. Different ergonomic aspects can be improved through that. While using the object one can understand and improve the human - object relation. We used this methodology to construct a more abstract idea. Everyone of us invented a magical machine based on a human desire or need.

The first need I have picked was 'vengeance' - the need to strike back. I am a person who very seldom seeks revenge. Rather than confronting an unpleasant person with the same hate they emit, I rather offer them cheerfulness. Every-time someone is not nice to me, I will be extra nice to them. This is one way of protecting myself and it makes the other person realize that (at least to a certain extend) that it makes no sense to act like this. For that I invented a *happy canon* which is a wearable placed on the shoulder. Every-time the user is confronted with an unpleasant person, the person will be bombarded with joy and confetti.  

![]({{site.baseurl}}/LWIPhoto02.jpg)

![]({{site.baseurl}}/Photo03.jpg)

The second human need I picked was based on my current project idea. 'Social Contact' - The need for relationship with others. I want to work on human - digital - ai communication. On the one hand I am interested and how we can communicate. On the other hand I am very interested in *translation of information*. The crux of the matter is 'other'. I think other does not have to be another human in this case. It could also be something artificial. How could we *communicate with an artificial intelligence network* which is not physical? Through which materials could we communicate? We have worked in a group on different scenarios how humans could interact with AI. One idea is to see AI as something that surrounds us, rather than a killer robots, something like network. And that the relation to it could be more intimate if the communication 'device' would grow with us, in some organic form, linked to our human system. It would be able to evolve on its own. Since our birth it would be connected with us through a ritual. The appearance would be subtle, placed in your hand. With a touch we could connect to the network, *exchanging knowledge and information*.

![]({{site.baseurl}}/LWIPhoto03.gif)

### Communication

Communication is an essential part of our society. The past years the rapid development of communication devices has almost wiped out any hand written letters. It became natural for us to communicate mostly through our phone. Social media, WhatsApp or other instant message providers rule over our communication velocity and style. Technology has an evident impact on our communication. Just as the technology becomes better and better, communication seems to improve as well. What's sad about the digitalization of our words is that it looses all its character. A handwritten letter implies emotions and shows effort. A message written while being in the bus, listening to a podcast and looking out of the window and autocorrect is taking care of the rest, does not have the same mindfulness as a letter. My intention is not that we should go back to the state, in which a man on a horse, should deliver our written thoughts. My concern is that we sometimes don't value the tools we have. We are constantly over-stimulating our brain. Everything happens in our small little bubble, thoughts and wishes stay in the digital world.

![]({{site.baseurl}}/LWISketch02.jpg)

The increasing correlation in our world is convenient and fast. And that is amazing! Through the internet we are able to stay in contact with loved ones who live on the other side of the globe. In case of emergency we are able to receive help, fast. These are developments we need and which will get improved in the age of Artificial Intelligence.

There are many examples in which social-media platforms, technology and artificial intelligence has helped in human or nature made disasters. I am thinking about two years ago when a man in Munich shot randomly into a mass of people at a shopping mall. The whole city was locked down. I was able to update my status in facebook to 'in safety during the shooting in Munich'. Friends and family from outside of Munich knew within minutes that I was safe. Without having to call every single one of my contacts. This is a small add on a huge platforms like facebook which can be helpful in disastrous situations. Several project like [NASA Finder](https://www.nasa.gov/jpl/finder-search-and-rescue-technology-helped-save-lives-in-nepal), [ Serval Project](http://www.servalproject.org), [Google Person Finder](https://google.org/personfinder), [Micromappers](https://micromappers.wordpress.com) and [TERA](http://www.ifrc.org/en/what-we-do/beneficiary-communications/tera/) (Trilogy Emergency Relief Application), are all communication devices using technology to help people in need. Where are future assignments for technology in communication? How can we add value instead of just making another gadget? How can we make sure that the information is transmitted correctly?

![]({{site.baseurl}}/LWISketch03.jpg)

To understand what and how we can improve while communicating with Artificial Intelligence, I have to understand what is not working at the moment. The only way how the digital world can communicate with us is through AI. But we don't exactly know what happens within this blackbox. The digital world is anonymous. We assume that the *other* person we are connected with, is also *human*. We never assume that whom we are a talking with could be something *different*. I am questioning the way of communication with the digital world. Are there new ways of creating an interface for AI? What are possible services AI could do for us, to help communicating between systems. How many people are actually using Alexa, Siri and Cortana? Are they actual helpful assistances? Do they understand our questions and intensions? For that I am currently living with two personal assistant systems: [Amazon Alexa](https://www.amazon.com/b?node=17934671011) and [Google Assistant](https://assistant.google.com/#?modal_active=none). They both have a similar approach and interfaces. For the next couple of weeks I am going to document my daily life with them. Investigating differences between them, writing down replies and interactions. The goal is to explore the field of AI communication and get inspiration for designing trust.

![]({{site.baseurl}}/LWISketch01.jpg)
