---
title: The Way Things Work
period: 18 November 2018
date: 2018-11-18 12:00:00
term: 1
img: images/Work-Cover.png
published: true
---
### Consumerism

When looking around yourself you may notice all the furniture, plants, pencils, cups and glasses. All the objects you have bought or someone bought for the office, cafe, restaurant or school. Now think about when you have bought them and how often you replaced them. We are living in a *throw-away society* today. We often replace our old shelves with a new IKEA PAX bookshelf. Changing furniture like fashion every season. I am not saying that IKEA is bad, they are designing nice and cheap furniture. Through that a well designed home is available for everyone. But we consume in such a fast pace that we forget that with object we are buying there are resources used. And these resources are not always from an environmental friendly source.

It is not a surprise that we buy that much *things*. Our whole life, every day we are bombarded with commercials. Telling us to buy the newest iPhone, a new yoghurt which is good for our health or new toys for our kids. These commercials are made really smart. They make us want what we actually don't need. It becomes such a normal part of our everyday life and we often don't see how these advertisements are *directly influencing our buying habits*. We also constantly compare ourselves to other people. We want to be 'part of the society', we want to belong. And somehow in our mind it is embedded that we have to have all these things other people have, to be part of the group. We forget that our society is based on more than materialism. We are a community. We can share knowledge and interests based on our culture or heritage. And for that we don't always need to have the newest phone or shoes.

### A look inside

While buying a new desk, do you know where it came from? Do you know the materials used and how the employees have been treated? Probably not. If we use electronics as the example it gets even worse. An iPhone is made out of an enormous amount of pieces. All coming from different manufacturers, assembled in China, far away from us western users. Beside consciously buying we sometimes should take a closer look. What materials have been used for the new gadget. How does it actually work? When we understand objects we can not only use them for their given purpose. We can hack them. Taking apart a broken printer or coffee machine can help understand how it functions. Even though a lot of electronics do not give us the opportunity to repair them ourselves, we should at least try where there is the possibility. In the world of *google* there is almost no information which can't be found. While taking apart a printer, we can find many electronics, coming from all over the world. The parts found can be used to create something new. Therefor we cannot only explore how things work, but we can find new purposes for them.

![]({{site.baseurl}}/Photo00.jpg)

### A natural Interface

The amazing company [Arduino](https://www.arduino.cc) is an open source platform for electronics. Through their project we are able to better understand electronics and can create a new object from scratch. How can we combine the world of electronics with the organic? Trees, flowers and fungi are often underestimated. We can't see their hidden networks. In a forest the [mycorrhizal](https://en.wikipedia.org/wiki/Mycorrhiza) network connects an enormous amount of plants. The fungi is able to create a network through the symbioses of roots and mycelium. Information, nitrogen, water, carbon and more can be transported through this. In our project we want to combine the two worlds, exploring networks and interfaces. We used different sensors implanted in the plant system to encourage the observer to interact with the plant.

![]({{site.baseurl}}/TWTW_Photo03.gif)

![]({{site.baseurl}}/TWTW_Photo01.jpg)

![]({{site.baseurl}}/TWTW_Photo02.jpg)
