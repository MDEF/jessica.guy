---
title: Library
period: 20 December 2018
date: 2018-01-01 12:00:00
term: 1
img: images/Library-Cover.png
published: true
---

## References, readings & state of the art related to my project. I added some interesting things!

### Data
- Read: [Our ears are unlocking an era of aural data](https://www.wired.com/story/sonification-era-of-aural-data/)
- Read: [Human-Centred Design in the Age of Data Connectivity](https://medium.com/@megukoyama/human-centred-design-in-the-age-of-data-connectivity-31bec8276e32)
- Read/ Project: [Can speculative evidence inform decision making?](http://superflux.in/index.php/speculative-evidence-inform-decision-making/#)
- Project: [Open Data Platform Helsinki](https://hri.fi/en_gb/2)
- Project: [The Air Quality Egg Learning System](https://airqualityegg.com/home) The Air Quality Egg is a learning system with which everyone can easily conduct real scientific experiments using real-time air quality data.
- Project: [The Smell of Data](http://www.nosenetwork.nl/the-smell-of-data-an-olfactory-warning-system-for-a-digital-world/?fbclid=IwAR0JOUvBwP_UxukDjjJhUD4i6-ML22yS5ZlbqlcBhQwtyyQLgvKbqmCK9Iw) an olfactory warning system for a digital world.
- Project: [Summer Streets Smellmapping by Kate McLean](https://sensorymaps.com/portfolio/summer-streets-smellmapping-astor-place-nyc/)
- Project: [Research and Data Investigation](https://labs.rs/en/)
- Project: [Dear Data](http://www.dear-data.com/theproject)
- Project: [Application for visualzing the infospehere of Radio](https://itunes.apple.com/cz/app/architecture-of-radio/id1035160239?mt=8)
- Project: [The White Spots App visualizes the invisible electromagnetic cloud ](https://itunes.apple.com/cz/app/white-spots/id1065857134?mt=8)
- Project: [WiFi Tapestry by Richard Vijgen](https://www.richardvijgen.nl/#the-deleted-city-3-0)
- Project: [SARoskop by Martin Hesselmeier](http://www.martinhesselmeier.com/saroskop)


### Synesthesia and Sensing
- Read: [Sense in the City: Urban Sensing as a Digital Intervention for Planning](https://medium.com/@dangerbui/sense-in-the-city-urban-sensing-as-a-digital-intervention-for-planning-5b88c2edea17)
- Read: [Designing for the Senses: Prologue](https://medium.com/@cantrell/designing-for-the-senses-prologue-7c15b364f01d)
- Read: [Immersion, the Senses, and Embodied Experiences](https://medium.com/a-series-of-epistolary-romances/immersion-the-senses-and-embodied-experiences-cb34b82252e)
- Read: [Sensing the immaterial-material city](https://medium.com/butwhatwasthequestion/sensing-the-immaterial-material-city-da268e540e86)
- Read: [Shape talking about seing and doing by George Stiny](http://shapetalkingaboutseeinganddoing.org/Shape.pdf)
- Project: [Synaesthetic olfactory device to perceive colour through fragrance](https://www.creativeapplications.net/objects/bouquet-synaesthetic-olfactory-device-to-perceive-color-through-fragrance/) Created by the ECAL’s Bachelor Media & Interaction Design students, and led by Niklas Roy, Bouquet is a synaesthetic olfactory device which allows the user to perceive color through fragrances.
- Project: [Sound, a tributary of Touch by Noah Silver](http://www.noasilver.com/portfolio/sound-a-tributary-of-touch/)
- Project: [Smog Tasting: Take Out by The Center of Genomic Gastronomy](http://genomicgastronomy.com/work/2017-2/smog-tasting-take-out/)
- Project: [Vienna Summer Scouts](https://johannapichlbauer.com/summer-scouts)
- Project: [L8266](https://cargocollective.com/evaporcuna/L8266)
- Project: [Painting Wifi Signals by Timo Arnall, Jorn Knutsen and Einar Sneve Martinussen](https://www.designboom.com/design/immaterials-light-painting-wifi-by-timo-arnall-jorn-knutsen-einar-sneve-martinussen/)
- [Domestic Data Streamers](http://domesticstreamers.com/) has a new [Project](https://www.instagram.com/p/BvlzxjKhtkI/) about Synesthesia for people with a taste disorder. In colaboration with a Chef they created a new sensorial experience which could amplify tasting abilities.
- [Mary Hallock Greenewalt’s Illuminated Music](https://daily.redbullmusicacademy.com/2017/09/mary-hallock-greenewalt-feature) Conceptually ambitious and technically innovative, an advanced system for syncing light and music.
- Watch: [How sonar can be used to navigate the world as a blind person](https://www.ted.com/talks/daniel_kish_how_i_use_sonar_to_navigate_the_world)
- Watch: [David Eagleman on Can we create new senses for humans?](https://www.ted.com/talks/david_eagleman_can_we_create_new_senses_for_humans?language=en#t-1017466)

### Perception
- Read: [The why of reality](https://aeon.co/essays/can-a-philosopher-explain-reality-and-make-believe-to-a-child?utm_source=Aeon+Newsletter&utm_campaign=b109279b4a-EMAIL_CAMPAIGN_2019_02_06_03_04&utm_medium=email&utm_term=0_411a82e59d-b109279b4a-70648145) Can a philosopher explain how and what reality is to a child?
- Read: [How Technology Changes Our Perception of Time](https://thriveglobal.com/stories/how-technology-changes-our-perception-of-time/)
- Read: [The Tree of Knowledge](https://www.cybertech-engineering.ch/research/references/Maturana1988/maturana-h-1987-tree-of-knowledge-bkmrk.pdf)
- Read: [Lines by Tim Ingold](https://www.goodreads.com/book/show/979759.Lines)

### Attention
- Read: [Attention is not a resource but a way of being alive to the world](https://aeon.co/ideas/attention-is-not-a-resource-but-a-way-of-being-alive-to-the-world?utm_source=Aeon+Newsletter&utm_medium=email&utm_campaign=GENTLE_READER_2018_12_07)
- Project: [Curated Self by Florian Semlitsch](https://designinvestigations.at/diplomas/curated-self/) Devices that mediate smartphone consumption. He looks into mediation of technological consumption and the attention economy.
- Watch: [The Divided Brain and the Unmaking of Our World by Iain McGilchrist](https://www.youtube.com/watch?v=5Q2XzLvuJWc)

### Olfactory
- Project: [Olfactory Education by Jonathan C. L. Chan at the RCA](https://www.rca.ac.uk/showcase/show-rca/jonathan-chan/)
- Project: [Amy Radcliffe about Scent-ography](https://www.wired.com/2013/07/this-machine-is-a-camera-for-your-smell-memories/) A Odor camera turns your favorite smell into memories.

### Interspecies and Identity
- Read: [A rock, a human, a tree: all were persons to the Classic Maya](https://aeon.co/ideas/a-rock-a-human-a-tree-all-were-persons-to-the-classic-maya?utm_source=Aeon+Newsletter&utm_campaign=2af5e32ac6-EMAIL_CAMPAIGN_2019_04_18_06_01&utm_medium=email&utm_term=0_411a82e59d-2af5e32ac6-70648145)
- Read: [Who Sees Human? The Stability and Importance of Individual Differences in Anthropomorphism](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4021380/)
- Read: [What does it mean to be human?](https://www.psychologytoday.com/us/blog/philosophy-dispatches/201205/what-does-it-mean-be-human)
- Project: [In the Eyes of the Animal](https://www.creativeapplications.net/maxmsp/in-the-eyes-of-the-animal-mlf-virtualise-a-forest-through-the-eyes-of-its-creatures/) MLF virtualise a forest through the eyes of its creatures. The [Website](http://iteota.com/experience/start-your-journey) visualises this journey.
- Project: [Alt-C which makes plants "talk"](https://www.creativeapplications.net/vvvv/alt-c-designing-for-synergy-between-our-ecosystems-and-network-economics/) Designing for synergy between our ecosystems and network economics.

### Communication
- Project: [Isabel Prade from the University of Applied Arts in Vienna](http://designdecode.org/article.php?p=isabel-prade&fbclid=IwAR3dgrOzRCLE0hx03i4P6NvMIq_FZ8qjV4XMNWFX0TeCyb4SWJZqIbflzXQ) How would communication look like if we would start creating new languages by using other senses than usual to express ourselves and understand each other?

### Technology and Artificial intelligence
- Read: [New AI fake text generator may be too dangerous to release, say creators](https://www.theguardian.com/technology/2019/feb/14/elon-musk-backed-ai-writes-convincing-news-fiction)
- Read: [The State of AI](https://www.theverge.com/2019/1/28/18197520/ai-artificial-intelligence-machine-learning-computational-science)
- Read: [Communication and Artificial Intelligence: Opportunities and Challenges for the 21st Century](https://scholarworks.umass.edu/cpo/vol1/iss1/1/)
- Read: [In the Age of A.I., Is Seeing Still Believing?](https://www.newyorker.com/magazine/2018/11/12/in-the-age-of-ai-is-seeing-still-believing)
- Read: [Does my algorithm have a mental-health problem?](https://aeon.co/ideas/made-in-our-own-image-why-algorithms-have-mental-health-problems)
- Read: [An Ethical Framework for a Good AI Society: Opportunities, Risks, Principles, and Recommendations](https://link.springer.com/article/10.1007%2Fs11023-018-9482-5)
- Read: [5 Ways mother nature inspires artificial intelligence](https://towardsdatascience.com/5-ways-mother-nature-inspires-artificial-intelligence-2c6700bb56b6)
- Read: [Constitutional Democracy and Technology in the age of Artificial Intelligenc](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3234336)
- Read: [The age of cyborgs has arrived](https://www.nextnature.net/2018/10/age-of-cyborgs/)
- Read: [Paul: On Dublin, Amazon, and the "Secret Region"](https://www.dublininquirer.com/2018/11/28/paul-on-dublin-amazon-and-the-secret-region)
- Project: [Connected Tools by Giulio Barresi at ECAL](https://www.creativeapplications.net/vvvv/alt-c-designing-for-synergy-between-our-ecosystems-and-network-economics/) Devices that mediate smartphone consumption. He looks into mediation of technological consumption and the attention economy.
- Project: [AI Serve](https://www.aiserve.co/) AiServe helps blind and visually impaired to navigate safely and independently. Also related to Synesthesia.
- Project: [AMI with Google](https://ami.withgoogle.com/)
- Project: [Snips](https://snips.ai/)Snips is a decentralized Voice Assistant technology.

### Climate change and the Anthropocene
- Data Visualisation: [These tools to help visualize the horror of rising sea levels](https://www.theverge.com/2019/2/17/18223808/climate-change-sea-level-rising-data-visualization-environment)
- Read: [Air pollution may be making us less intelligent](https://www.nextnature.net/2019/01/air-pollution-may-be-making-us-less-intelligent/)
- Read: [Measuring the economy in the age of digitalisation](http://oecdobserver.org/news/fullstory.php/aid/5679/Measuring_the_economy_in_the_age_of_digitalisation.html)
- Project: [Indy Johar Exhibtion "We are they"](https://www.royalacademy.org.uk/event/indy-johar-in-conversation) What does it mean to be human? An Exhibition at the Royal Academy of Arts in London
- Project: [Artwork lets trees tell the story of climate change by Thijs Biersteker](https://www.nextnature.net/2018/12/artwork-lets-trees-narrate-climate-change/)
- Watch: [Charles Eisenstein Interview from Living the Change](https://www.youtube.com/watch?v=ggdmkFA2BzA&feature=youtu.be)

### The City
- Read: [Legitimate Change & The Critical Role of Cities](https://provocations.darkmatterlabs.org/legitimate-cities-df8f5561780e)
- Read: [The City belongs to us](https://aeon.co/essays/cities-thrive-when-public-space-is-open-to-all)
- Read: [Modern day Flâneur](https://aeon.co/essays/how-i-learned-to-love-new-york-city-stride-by-stride)
- Project: [Interactive Maps That Reveal What Cities Sound Like](https://www.citylab.com/life/2016/03/these-colorful-maps-reveal-what-cities-sound-like-chatty-maps-good-city-life/475224/) Also related to Data and Sensing.
