---
title: Make Here
period: 29 January 2019
date: 2018-12-31 12:00:00
term: 1
img: images/Makehere.png
published: true
---

<iframe src="https://player.vimeo.com/video/310756502" width="680" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


[Make Here](https://makehere.gitlab.io/make-here-bcn/) is a project to create a community around local production to share knowledge, information, ideas and recommendations in a city or neighbourhood. We want to connect people who make to people wanting to make. Make Here started as an one-week project by students of the Master in Design for Emergent Futures (MDEF) @ IAAC - inspired by Make Works and Fab City. We want it to spread globally. We initially mapped information about maker-spaces, material suppliers, workshops and other creative spaces in Poblenou, Barcelona. The concept is: anyone can go through the same process, developing this project in their own cities through the aid of this toolkit. A global network can be developed in which these different communities learn and thrive off each other. Therefore, laying the foundations for change towards a more circular economy, and rethinking our relationships with local production.

![]({{site.baseurl}}/images/MakeHere_Reflection1.png)

![]({{site.baseurl}}/images/MakeHere_Reflection2.png)

![]({{site.baseurl}}/images/MakeHere_Reflection3.png)

Make Here was a great opportunity for me and my fellow students. We enjoyed the week exploring the hidden gems of our neighbourhood. We often scroll through our cities without knowing where the local maker community is and who they are. We used simple mapping tools and the makers guided us through the streets by the word of mouth. We started of talking to the makers we have met beginning of our master. A smile and a nice greeting often was enough to start a conversation. We have heard the pros and cons of Poblenou, which gave us a greater understanding of the local economy. We are sharing our methodology online, so anyone anywhere can copy at and share their experience. The goal is to share information, get people involved and strengthen local communities. Overall the project was a great success, that is why we want to continue working on it as a team.


Check out the rest of the Make Here Team: [Veronica](https://mdef.gitlab.io/veronica.tran/), [Emily](https://mdef.gitlab.io/emily.whyman/), [Ola](https://mdef.gitlab.io/ola.lukaszewska/), [Gabriela](https://mdef.gitlab.io/gabriela.martinez/) and [Alex](https://mdef.gitlab.io/alexandre.acsensi/).

Our weekly updates can be seen on Instagram @[Make Here](https://www.instagram.com/makeherebcn/)
