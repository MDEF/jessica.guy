---
title: From Bits to Atoms
period: 16 December 2018
date: 2018-12-16 12:00:00
term: 1
img: images/Atoms-Cover.png
published: true
---

There are no more excuses. We have a visual and physical irreversible impact on nature. We used to call this realm surrounding us 'mother nature'. Would you spill oil on your mother? Or rip out parts from her to make things we actually don't need? I don't think so! We exploit our environment and with it also our own species. The subtle changes in our environment are not obvious enough for our phone focused minds. Rising sea water levels, more droughts and the extinctions of creatures surrounding us are not enough to awake. One of my favourite movies [Mononoke](https://www.imdb.com/title/tt0119698/) is a wonderful example on how nature and humans should learn to live together in a more balanced way. One of the gods in this movie says: "The trees cry out as they die, but you cannot hear them". And that is very true. How could we give nature a voice? How can we make humans listen?

![]({{site.baseurl}}/BAPicture02.jpg)

Our Project "Sassy Plants" is a playful way of giving nature the tool to intervene in our life. The Sassy Plant can follow humans movements. It will survey your every step, creep up to you and tell you off! Trying to make sure that you won't destroy nature even more. Sassy plant currently still needs a human operator. But in the future it will be able to move on its own free will. Occasionally poking you with its weapons of annoyance. The plant cyborg gives nature the agency to fight for itself. And serves as a reminder that we should respect nature.

![]({{site.baseurl}}/BAPicture03.JPG)

For the project I helped with the general design of the plantbots body. Making sketches and building mockups. Together we made the first mockup using cardboard and an electric toy car. We first thought about using wood to build the plantbot. But as is turns out cardboard was the perfect material. It is controversial though, using a dead plant (cardboard) to move the living plant. The assembly of the electronics and the cardboard body was done in teamwork. Kat, Fifa and I made a storyboard, wrote a script and filmed fellow classmates for our final presentation.

<iframe src="https://player.vimeo.com/video/307704303" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
