---
title: Design for the new
period: 30 January 2019
date: 2019-05-01 12:00:00
term: 1
img: images/Designforthenew.png
published: true
---

## Class content

![]({{site.baseurl}}/images/LevelofEffectiveness.svg)

![]({{site.baseurl}}/images/Triangle-01.svg)

![]({{site.baseurl}}/images/Triangle-02.svg)

## Intervention Prototype

My goal for this master is to make an electronic sensing organ which can be used by dwellers of the city. I want this to be used longterm and the experience should be pleasure rather than a negative feedback. There is no point of making an object which will give people a negative feedback for their actions. Taking my past experiences into account people will just get rid of something which will make them feel bad. But an object which makes data experienceable in real time, will help to understand the city in a new way. The moment in which this happens is between data and information. One possible outcome is a toolkit which will guide the user into making their own organs. Interesting would be an online map of real time phenomenological experiences, to see where interesting data sensing stimulation happens in the city. Which would also mean that there is a meta data hub for environmental issues. The result is that we can gain tacit knowledge about the city dynamics and it will expand our view of reality.

### Why EMF?
Looking at the electromagnetic spectrum it is clear that all frequencies, may it be light, radio, ultraviolet or gamma rays are surrounding us at all times. The only part which we are able to see is light, which we see as colour, and this a fraction of the spectrum.

*What else is surrounding us what we cannot see? And how does it influence us?*

![]({{site.baseurl}}/images/EMF.svg)

For the design of the artefact or electronic sensing organ, I have decided to divide between the working electronic prototype and a speculated concept design. This gave me the freedom to experiment and speculate more with the design without being constraint with the electronics.

To find the right design I have to explore body, material and context. And then revisit ideas over and over again, to be tested not only by me but also by others.

![]({{site.baseurl}}/images/Organ_Concept00.png)

![]({{site.baseurl}}/images/Organ_Concept01.png)

![]({{site.baseurl}}/images/Organ_Concept02.jpg)

I want to sense electromagnetic frequencies. For that I am using a design from the Lab 3 The Laboratory for Experimental Computer Science at the Academy of Media Arts Cologne which will serve as my input. The PCB contains a LT5534 sensor chip, a variaty of components and antennas. The lengths of antennas determines which frequencies can be read. The antenna is a piece of wire which will be connected by a terminal block to the PCB to be able to exchange the antennas.

The output will be several vibration motors which will be connected to an arduino via Mosfets, which make it possible for me to trigger each motor individually. I have choosen vibration as the output of the organ because it translates the signals directly onto our skin. It will make it possible to sense our environment without having to look, read or touch something. After living with the organ it is possible to learn the "language", which we can understand as tacit knowledge. Vibration is already being used to translate the environment for the visually impaired. I have tried out Eccentric Rotating Mass vibration motor (ERM) as well as Linear Resonant Actuators (LRA) as possible outputs. I have decided to use the LRA’s as they overall size and feeling of the vibration work well for my needs.

![]({{site.baseurl}}/images/Electronic_Schematic.svg)


The schematic for the EMF detector, the 3D design and a Step by Step tutorial can be downloaded from my [Gitlab Repo](https://gitlab.com/JessicaGuy/sensingtheinvisible/tree/master)

The next step is to make a fusion of the concept design and the working prototype. This might not be the final version but at least an exploration of how both elements could work together.

## Reflection

The class "Designing for the New" with Markel Cormenzana and Mercè Rua were very interesting. Both of them gave us as a group needed clarity about our own project. During the classes, we have unpacked the terms and words we have been using for a long time, without clarifying their meaning. They unravelled with us together with our practices within our interventions. Both of them were very patient about helping us, even at times when we have been insecure or unsure about where our projects were heading.

We have been defining our practices and defined our pathway to the future. Speculating events which will have to happen or might happen to elevate the intervention/ practice was very interesting tasks. It helped me to narrow down and actually think about what has to happen in the future to make the future that I am imagining happen. I have been thinking that it is obvious that my project would make sense for everyone. But this class also showed me that have to be critical about what I am doing. There are multiple layers - macro & micro - in reality, which can enable further development. Overall I am very happy with the outcome of this class, even though I am convinced that the class would have been more helpful in the second trimester. The structures given by Makel and Mercè are definitely something we have needed earlier.
