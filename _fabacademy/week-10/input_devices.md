---
title: Input devices
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-11.svg
published: true
---

## Notes from the class
How intelligence work in natural systems, Inputs are being processed and then an output is chosen. Thats how biological intelligence is formed. In engineering and software all keyboards, mouses etc are inputs devices and printer etc. are output devices. Environmental sensors are all over in nature. For example Lichens are able to sense air quality and change accordingly their color. To measure phenomenas around us something which ha always been around. Everything that happens in our environment will be a form of energy, this energy we are able to translate into mechanical and digital devices. Today we are storing the data as a value and share this information.

The biggest use of sensors is not only to measure the environment but too close the information loop with outputs. Devices use inputs and outputs to adjust their system. For example in heaters or projectors. These are called closed loop systems. Sensors are becoming cheaper and cheaper. We can use sensors in control systems to make them even preciser. Open loop systems like 3D printers are also using sensors to a certain extend. The 3D printer uses a sensor to know where the zero is on the build plate. Data fusion will combine the information from many sensors. When thinking about designing with sensors we have to take into account that sometimes we are able to use less sensors than we think.

How to choose a sensor?
A webcam can already be a of the shelve sensors including a USB connection. It uses 600 x 400 pixels which can be used as a color sensor. There are many devices already on the market which are cheap and affordable which we can use, but they will need a lot of coding.

To make all these things possible we have to rely on capitalism. At leat for now. All the amazing parts like compasses and gyroscopes which used to be huge, have only become so tiny because of capitalism. The need for these objects in nano sizes is based on big companies seeing a possibility to make money from it. For example they have been used in the first Wii. Then the drone community disassembled the controllers and used them to make drones. Now we have tiny gyroscopes which are being used in our phones but also for great projects. Nano technology sizes the mechanical processes down on to a chip, this is called MEMS.

## Week assignment
For this weeks assignment we have to use an existing PCB design, mill, solder, program and read the data through the serial monitor. I will try to make the first "mockup" of my electronic organ for my master project. I want to sense electromagnetic frequencies. For that I am using a design from [the Lab 3](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/). The lengths of antennas determines which frequencies can be read. The antenna is a piece of wire which will be soldered to the the PCB. I will add a RF connector to be able to exchange the antennas.

![]({{site.baseurl}}/images/lt5534.jpg)

I have used the original schematic as reference and added the RF connector.
[Here](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/) a link to the original files.

For the sake of the design I have decided to change the double-sided PCB to an one-sided PCB. I have checked with on of the local instructors to get feedback on the layout/ design of the PCB. We have added a [terminal block](https://www.mouser.es/ProductDetail/TE-Connectivity/284391-2?qs=sGAEpiMZZMvZTcaMAxB2AM0YT%252BRZsuh%2FnVGk0Jv0jjQ%3D) 3.5 mm with 2 connections (A1-A2 connection was then replaced with this pitch). This allows me to exchange the antennas without having to resolder them and use only one PCB.

![]({{site.baseurl}}/images/190517_Input01.png)

We have exchanged all the components to Fabmodules ones, to make sure that there will no problems when milling. After I have checked the file through the DRC. It showed that there will be errors with the components itself. Unfortunately that is not something I can fix in Eagle. Therefor I have opened the file in Fabmodules to simulate and check if the traces will be milled. If not there is still the possibility to change the width of the line manually in Photoshop.

![]({{site.baseurl}}/images/Input_dimensions.png)

![]({{site.baseurl}}/images/Input_traces.png)

The results are these two jpegs which then are open with the Roland software on the computer.

![]({{site.baseurl}}/images/190517_Input02.png)

I have set the x/y and y axis manually on the lower corner of the PCB. At first the machine did not mill/ entered the material at all. I have checked the end mill but it did not break. I went back to the origin and started the milling again, yet there was not significant milling happening. I decided to go to the home and let the machine mill on the spot. I have the set speed to 50% and then added another 0.1 mm to the z axis.

![]({{site.baseurl}}/images/190520_Input12.jpeg)

We have encountering the same problems as in the electronics design week. When exporting from Eagle with a Mac the PCB turned out to be twice the original size. To check the size in Eagle you can click *Tools* - *Statistics*, and then resize it manually in Photoshop.

![]({{site.baseurl}}/images/190517_Input03.png)

Unfortunately after changing the jpeg the file changed for some reason, through that the Z was completely wrong. We have done the whole process two more times, but nothing changed. The machine kept on milling in the air.

![]({{site.baseurl}}/images/190517_Input05.jpg)

I switched over to using the vinyl cutter as I am planing to make an electronic organ out of this input anyway. For this I had to change the jpegs slightly. Instead of having the file for the holes, I made one which shows only the pads on which the components will be soldered. This can be easily done by opening the original file in photoshop and blackening out all the traces and leaving the pads. This file can be cut in the second layer material and will make sure that the copper traces will not detach from the PVC.

![]({{site.baseurl}}/images/190520_Input13.jpg)

![]({{site.baseurl}}/images/190520_Input14.jpg)

![]({{site.baseurl}}/images/190520_Input15.jpg)

The adhesive coper tape can easily sticked on the PVC, it works best doing this in pairs. One person can hold the copper and slowly unroll it, while the other one uses a straight and hard tool (like a ruler) to press the copper on evenly. The goal is not to get any bubbles or wrinkles.

![]({{site.baseurl}}/images/190520_Input06.jpg)

Then I have added the sheet to the Roland Vinyl cutter. After turning it I selected *piece* instead of *roll* which will trigger the machine to measure the sheet. When the machines has measured the piece I pushed the *origin* button which will safe the size of the piece. In the software I have open *Cutting* and then clicked *properties* to get the software to use the size of the sheet.

![]({{site.baseurl}}/images/190520_Input07.jpg)

I had to play around with the settings of the machine a little bit to find the right variable for cutting the copper. The best setting was 170/180 g and about 1 for the cutting depth/ force.

![]({{site.baseurl}}/images/190520_Input08.jpg)

Then I used tweezers and a carpet knife to take off the access copper tape. This has to be done very carefully otherwise the fragile copper traces can be easily taken off as well.  

![]({{site.baseurl}}/images/190520_Input09.jpg)

I have cut several of the ones which turned out nice. The ones which did not turned out be nice I will use for testing the soldering and the second layer.

![]({{site.baseurl}}/images/190520_Input10.jpg)

In the next step I have cut the second layer of adhesive tape. I have not changed the settings for the machine, but the cutting turned out nice. I then carefully sticked on the second layer to one of my "tryout" pieces. It turned out well. I used this piece to test soldering the antenna connector and resistor.  

![]({{site.baseurl}}/images/190520_Input11.jpg)

To make sure that the plastic is not melting I tried out several heating settings. Setting the solder-iron to a high heat will make the solder melt quickly. But this also means that I have to solder very quickly. If the heat would be lower the iron is more likely to melt the plastic. It was very hard to solder the sensor chip as the the pins and the chip itself are tiny. I needed a second try to make it actually work.

![]({{site.baseurl}}/images/Input01.jpg)

### Programming

The code is the original one from the [The Laboratory for Experimental Computer Science at the Academy of Media Arts Cologne](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/)

 ```c#
 /* LT5534 RSSI Sensor test
 * Lab3 2010
 * Kunsthochschule fuer Medien Koeln
 * Academy of Media Arts Cologne
 * http://interface.khm.de
 */

int analogIn = 0;
int analogValue = 0;
int pinLed = 13;
int cnt;

void setup(){
  Serial.begin(57600);
  pinMode(pinLed,OUTPUT);

}

void loop(){

  analogValue=0;
  for (cnt=0;cnt< 100;cnt++) {
    digitalWrite(pinLed,1);
    analogIn=analogRead(1);
    digitalWrite(pinLed,0);
    if (analogIn > analogValue) analogValue=analogIn;
    delayMicroseconds(100);

  }
  Serial.println(analogValue);
  delay(100);

}
```


All the files can be found [here](https://gitlab.com/MDEF/jessica.guy/tree/master/_fabacademy/week-10).
