---
title: Networking and Communications
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-13.svg
published: true
---

## Notes from the Class

### Asynchronos Communication

- *RX/TX* Serial communication which uses two wires (seen in Attiny 44 etc.)
- *UART* stands for Universal Asynchronous Receiver/Transmitter
- USB 2.0 500 milli ampers/ 480 megabits per second (Mbps)
- USB 3.0 1000 mili ampers/ 4,800 Mbps per seconds (Mbps
- *Diodes* A diode is a two-terminal electronic component that conducts current primarily in one direction; it has low resistance in one direction, and high resistance in the other.
- *Zener Diodes* A Zener diode is a type of diode that allows current to flow not only from its anode to its cathode, but also in the reverse direction, when the Zener voltage is reached. Not for regulating power but for regulating data.

### Synchronos Communication

- I2C  (Inter-integrated Circuit) Drones uses I2C to control everything. I2C can also be used to talk between different sensors/ Arduinos/ microprocessors. We can use one Master Arduino to control 200 Slave Arduino for example in installations
- *Master and Slave* Slaves can only add data to the data line - Masters can modify the data line. Example: Barometer sensor and laser distance sensor
- *SPI* Serial Peripheral Interface
- *AM Radio* amplitude modulation, 10 kHz Bandwidth and Frequency from 550 to 1720 kHz with 106 possible bands
- *FM Radio* frequency modulation, 200 kHz Bandwidth and Frequency from 88.1 to 108.1 kHz for 100 possible bands
- *Fresnel Zone* is the zone in which the waves are being transmitted
- *NATO phonetic alphabet* officially denoted as the International Radiotelephony Spelling Alphabet, which is used to make sure that data, even if parts are being lost in transmission, will be still identify the data.
- Lower frequencies (used for example in the marine) can more easily pass through materials, like walls. High frequencies like Wifi is faster but have more difficulties passing through walls. Thats why the further you are away from the router, the worse the signal gets.

### WiFi Communication
- You need an *acces point* (router) which is using a direct wire connection to the internet and transmits the internet to your station (For example ESP8266). The phone can then connect to that station.
- A *soft accespoint* (soft AP) for example a ESP8266 (which does not have a interface) can be used to directly connect to the internet without being wired to it. A soft access point can also be a station, which will make it to a node in a mesh network.
- *Client* is a mode in which a soft access point can be. It can then access data/ services provided by servers.
- *Server* is a mode in which the device can provide data for other devices. This is what we use when we make a local server to run our website with the command *bundle exec jekyll serve* with the static website generator jekyll.
- The pros of Wifi are that it is encrypted and is modulated in packages so you do not loose data.

### NRF24 Communication
NRF24 is a Transceiver and not a receiver transmitter. It has a package receiving confirmation. It will send the data over and over again until the other device receives the data package. It is not using serial communication but it sends data very precise.

### HC12 Communication
HC12 is a low frequency device which sends with half duplex wireless serial communication.

*lower baud rate less probability to loose data, and further range*

## Making a Network at IAAC
The goal of this weeks assignment was to set up a network in our classroom. We have used one Rasberry Pi with an I2C and HC12 which sends the data of the humidity and temperature sensor to the bridge. The Bridge uses an Arduino Uno, a HC12, RF24 to send the information to the nodes. The nodes are using a Arduino Uno with the RF24. We had one group which worked on the bridge, and three groups for the nodes.

Firstly we have connected the Arduino Uno to the RF24 following the Hackmd [example](https://hackmd.io/s/B15x5dn9V)

![]({{site.baseurl}}/images/Networking01.png)

![]({{site.baseurl}}/images/Networking01-1.jpg)

Before I can add any code I have to make sure that the Board and Port are set up.

![]({{site.baseurl}}/images/Networking02.png)

Then I have opened Arduino on my computer and downloaded the RF24 library. Clicking *Sketch* then *Include Library*, *RF24*.

![]({{site.baseurl}}/images/Networking03.png)

![]({{site.baseurl}}/images/Networking04.png)

In the first round we have used the *Sender* code from [Hackmd](https://hackmd.io/s/B15x5dn9V) to send a message to Broker. This has worked with the first try.

 ```c#
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);

// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;

char data[16]="Hola mundo" ;

void setup(void)
{
   radio.begin();
   radio.openWritingPipe(pipe);
}

void loop(void)
{
   radio.write(data, sizeof data);
   delay(1000);
}
```

We have used the following *Receiver* code from [Hackmd](https://hackmd.io/s/B15x5dn9V). For some reason we were not able to compile the code to the Arduino. We have tried several times. Changing parts of the code and downloading another RF24 library, but the error with the library kept showing up. We have changed the computer, everything worked out well. We were not able to find the problem with the first computer. But we guess that when that computer "blew up" a few months ago it ruined the USB port in a way.

 ```c#
   #include <nRF24L01.h>
   #include <RF24.h>
   #include <RF24_config.h>
   #include <SPI.h>

   const int pinCE = 9;
   const int pinCSN = 10;
   RF24 radio(pinCE, pinCSN);

   // The Address name can be anything you want.
   // We simply exchanged the 8 to a 6 in the Address.
   // Single radio pipe address for the 2 nodes to communicate.
   const uint64_t pipe = 0xE6E8F0F0E1LL;

   char data[16]="Hola mundo" ;

   //Adding the speed for reading the Serial monitor
   void setup(void)
   {
   	Serial.begin(9600);
      radio.begin();
      radio.openWritingPipe(pipe);
   }

   //Adding the Serial Print ln
   void loop(void)
   {
      radio.write(data, sizeof data);
      Serial.println(data);
      delay(1000);
   }
   ```

We gave every group an address number between 0x000001 and 0x00004. No messages showed up when opening up the Serial monitor. The baud number in the serial monitor and the code was not the same. After changing it we were able to receive all the messages from the Bridge.

![]({{site.baseurl}}/images/Networking05.png)

![]({{site.baseurl}}/images/Networking07.png)

![]({{site.baseurl}}/images/Networking06.png)

The next step was to send data in between two Arduinos and Transceivers in our group. We connected one Arduino with the RF24 to my computer and the other Arduino + RF24 + sound sensor called the hw484 (as an input) to Saira's computer. Before we add and output we wanted to make sure that sending the data works. We used [this page](http://osoyoo.com/2017/07/arduino-lesson-sound-detection-sensor/) as a reference for connecting the sound sensor to the Arduino. More information on the sensor can be found [here](https://randomnerdtutorials.com/guide-for-microphone-sound-sensor-with-arduino/). We used the code from [Adafruits](https://learn.adafruit.com/adafruit-microphone-amplifier-breakout/measuring-sound-levels) which also returns the maximum value per sweep.

The first problem we had was that the reading of the sound did not change in the serial monitor. The solution was to turn the screw on the sensor to tweak the voltage output so we could see fluctuations. Is is a small range of turning that is actually readable. For our sensor you turn anticlockwise for more voltage output.

![]({{site.baseurl}}/images/Networking09.png)

Secondly we had troubles with the sender code. We were getting error messages that the libraries where not found or not compatible. Ultimately we had to update the NRF library because it was buggy. The new copy of is compiling successfully and is uploading to the Arduino Uno.

![]({{site.baseurl}}/images/Networking08.png)

We have changed the receiver code from [Hackmd](https://hackmd.io/s/B15x5dn9V) slightly as we had problems compiling the sketch. The error message we got was "void value not ignored as it ought to be".

The original code had the line:

```c#
int done = radio.read(data, sizeof data);
```
and we changed it to:

```c#
radio.read(data, sizeof data);
```

![]({{site.baseurl}}/images/Networking10.png)

I have then opened the Serial monitor and was able to receive the "hola mundo" message from Saira. Then we wanted to set up the Arduino to read the sound sensor/ microphone, which gets translated into volts and send it through the network to the second Node. We had problems receiving the voltage data from the sensors. I was only receiving the "hola mundo". Nothing showed up in the serial monitor as soon as we changed the code to send the volts.

![]({{site.baseurl}}/images/Networking13.jpeg)

We were not able to find anything online to solve this problem. We had to get help from our local instructors. We added a code to translate the voltage into numbers which then can be send and read by the second node. For this we changed the voltage value which was a number into a text string array which we called "b".

```c#
char b[16];
String str;
str=String(volts);
str.toCharArray(b,16);
```

The full sender code is [here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-13/190508_CodeSender.ino.zip)

Saira's Computer with the sender code
![]({{site.baseurl}}/images/Networking12.png)

My Computer receiving the data
![]({{site.baseurl}}/images/Networking11.png)

![]({{site.baseurl}}/images/Networking14.jpeg)
