---
title: 3D Scanning and Printing
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-06.svg
published: true
---

This weeks first assignment is to design and 3D print something which can not be made with a CNC. The second task it so 3D scan an object with the Kinect.

### Printing
There are several Softwares which can be used for the 3D design. I feel most comfortable with Rhino, that why I have chosen to use it for this weeks assignment. The goal is to print something of use for our final project. I have decided to make a 3D printed fabric. Although I do not know exactly what I will do for my final project, I know that I want to explore wearables. For this I think a 3D printed material which can be made individually, with a perfect fit. There are several open source designs available. The chain mail is one of the best known designs. I made a simple square design inspired by designs I have seen online.

![]({{site.baseurl}}/images/3dmodel01.png)

First I have created the outlines of the design. The measurements are more or less guessed. What I was able to see in the original designs is that the squares sides have equal lengths. The space in between have to be slightly bigger than the thickness of the pillars. For example my pillars are 3 x 3 mm and the opening is 4.1 x 4.1 mm.

![]({{site.baseurl}}/images/3dmodel03.png)

The result is a cube with opening which allows the chains to move more or less freely. These then can multiplied with the command "array". Each pillar sits nicely in the openings of the cubes.

![]({{site.baseurl}}/images/3dmodel02.png)

It is important that there is enough space between each pillar of the cube, otherwise they will stick together after the print. I have chosen a distance of about one millimetre. After that the file has to be exported as a "STL", which is the "Standard Triangle Language". This allows any slicer software, in my case [Cura](https://ultimaker.com/en/products/ultimaker-cura-software), to read the file.

![]({{site.baseurl}}/images/Printing00.jpg)

I have used the Anycube to print the chain mail. The quality of that printer is very good. There are a few things which have to be taken into account when 3D printing. First of all it is sometimes good to print the design in a shitty quality first. That allows you to see the results quicker. After that the design can be send again, for example to print throughout the night, to have a better quality print. This means if you sacrifice time you can have a better quality. And if quality is sacrificed you can have a good result in a short amount of time.

![]({{site.baseurl}}/images/Printing01.jpg)

These are simple guidelines for 3D printing with Cura which can be followed, but adjusted for each print. The parameters dependent on the design.

- *QUALITY* Layer height 0.15 - 0.3; Initial Layer height 0.3 (to have a good base); Line width 0.4 or 0.6 (depending on the material diameter)
- *SHELL* The top layer should always be a little more than the bottom layer. Because when the print starts the printer bed gives the material support. But when finishing the top layer the only support the layers will have is through the filling structure.
- *INFILL* The percentage of infill should aways be around 20 - 90 % depending on the structural qualities the end product should have. There is a variety on infill patterns, but "cube" works fine almost anything.
- *MATERIAL* The material settings are determined by the material which will be used for the Print. The two materials manly used in out lab are PLA (Polylactic Acid) and ABS (Acrylonitrile Butadiene Styrene). PLA is a biodegradable thermoplastic filament made out of corn starch oder sugarcane. The cable roll of the filament will have temperature settings written on, this helps finding the right temperature for the extruder and Buildplate.
- *SPEED* The speed can be changed for example flexible filaments, otherwise it can happen that the filaments gets for example clinched.
- *TRAVEL* Sometimes it is more practical that the extruder travels around the object than above, to make sure that pieces are not hurt in the process.
- *SUPPORT* If the object has pieces which are not in between 90° - 45° or floating they need support to be printed. These support pieces then can be easily taken out if there is a small gap between the object and the support.
- *BUILD PLATE ADHESION* The first few seconds the extruder should print on the build plate. This makes sure that the filament is nice and even for the first round of printing. For this the setting 'Skirt' is best. Sometimes the design will need a supporting ground to make sure that it will stick to the build plate throughout the whole printing process. These setting will also be found in "Build Plate Adhesion"

Before sending anything out to print there is one thing which always has to be checked: That the design is a fully closed mesh, otherwise there will be problems during the print. And this is how the final print looked like:

![]({{site.baseurl}}/images/Printing02.jpg)

![]({{site.baseurl}}/images/Printing03.jpg)

![]({{site.baseurl}}/images/Printing04.jpg)


[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-05/190224_3DFabric.3dm.zip) a link to the files

### Scanning

The second task was to 3D scan an object. To scan with the Kinect the object has to be bigger than a 1 litre water bottle. Any reflective or transparent materials will be difficult to scan. There are two more options how one can 3D scan. One is the "milk version" and the other is with the small CNC milling machine. The CNC is very useful when scanning a small object, although it takes quite a long time.

For my project I would like to experiment with 3D printed textiles and wearables. That is why I have scanned my lower arm. I can then use the 3D model to design a wearable or 3D printed fabric/ skin which will fit me perfectly.

Settings for the software are pretty easy. In the first setting "Prepare" the size of the cube in which the object will be scanned can be chosen. The better the computers CPU / GPU is, the easier it will be to scan, otherwise you will have to move very slow with the Kinect. Even when making a very good scan there will always be the possibility that there are holes in the mesh. In this case there is a tool in the next step which is called 'fill holes' and 'watertight' which will close all holes in the mesh. In the end the object can be exported as an STL or OBJ to then either print it directly or do some modifications in a modelling software.

![]({{site.baseurl}}/images/Scanning01.jpg)

![]({{site.baseurl}}/images/Scanning00.jpg)

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-05/3DScanning.zip) a link to the files.
