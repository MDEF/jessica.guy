---
title: Electronics Production
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-05.svg
published: true
---
This weeks assignment was to make a PCB. A PCB is a printed circuits board which is mechanically/ electronically connecting electronic parts using e.g. copper as a conductive material. In order to that we first had to learn how to make a PCB from scratch.

![]({{site.baseurl}}/images/190219_Electronic04.jpg)

First of all we had a short introduction into the small CNC milling machines. These CNC's are able to mill very small components in comparison to the normal CNC. The drills for these are actually not called drills but "end mill". First the machines workspace has to be cleaned. After that the copper board can be sticked to the surface with double sided tape. The last step it to set up the "traces" and "interior" G-code files. It is best to use an offset of 4-6 for the lines, otherwise the lines will be very fine and difficult to solder.  

![]({{site.baseurl}}/images/190219_Electronic1.jpg)

The goal is to make the hello.ISP.44 board which can be found on the [Fab Academy electronics production](http://academy.cba.mit.edu/classes/electronics_production/index.html) webpage. The machine which I have used to mill my PCB has a maximum work area of the machine is 203 x 152 x 60 mm. It is fully enclosed, which reduces dust in the air. A nice feature is, when the front window is opened the machine automatically pauses. It is very important to not let the end mills fall into the machines nor on the floor, they can easily break.

![]({{site.baseurl}}/images/190219_Electronic00.jpg)

My PCB turned out very well. The interior lines could have been a little thicker to make the soldering easier, but I will have that in mind the next time I mill one.

![]({{site.baseurl}}/images/190219_Electronic03.jpg)

Fab Academy documentation offers an image which can be used a role model for the soldering. I've written down all the components needed in my sketchbook. Then I have after I have chosen the piece, I sticked it on the paper with tape. To keep the inventory updated it is very important to always add the pieces taken to the Exel inventory sheet of our local FabLab.

![]({{site.baseurl}}/images/190219_Electronic02.jpg)

Before soldering my actual PCB I have tried it a few times on some examples. It is good get to know the heat distribution of the solder iron. Also juggling the iron, the component and the solder takes some practice. I have started soldering from the middle. Step for step I have worked my way to the pieces on each end of the PCB. The most difficult one was the USB. It has very thin connection, which are close to each other. "The more solder the better", not really true in this case. Besides taking quite a long time I did not have any problems soldering. I find it very meditative. Before programming the board it is very important to check that all the connections are correct. This is done with a Multimeter. Every connection following the example should be checked, that means gowing from "GND" to "GND" and so on.

![]({{site.baseurl}}/images/190219_Electronic01.jpg)

![]({{site.baseurl}}/images/ElectronicComponents.svg)

### Programming the PCB

First step is to connect the FAB ISP to the computer with another ISP or ATAVRISP2. The  [FABISP Firmware](https://github.com/Academany/FabAcademany-Resources/blob/master/files/firmware_44.zip ) can be downloaded from Github and saved on the Desktop. After that I opened my Text-editor [Hyper](https://hyper.is/) and downloaded [Homebrew](https://brew.sh/). The command 'cd' can be used to make the firmware folder a directory. Then I followed step by step the tutorial for programming the PCB, typing "make clean", "make hex", "make fuse" and lastly "make program". In the first try my PCB did not show up as a USB on my Mac. After troubleshooting it was clear that I accidentally used the wrong resistor. I changed the resistor to the correct one and then the PCB worked. YAY!  

![]({{site.baseurl}}/images/Programming01.png)

![]({{site.baseurl}}/images/Programming02.jpg)
