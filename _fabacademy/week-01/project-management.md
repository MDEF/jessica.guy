---
title: Project management
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-02.svg
published: true
---

This weeks task is to set up a fab academy section within out existing website as well as getting to know the basic project management tools.
I decided to add the fab academy to my journeys, as I don't separate my website into the three semesters. I am differentiating the classes from last and this semester only visually by their covers within the journal.


### Understanding GIT

We already had the opportunity to get to know [GIT](https://git-scm.com/) during the first week of our master. Git is a distributed version control system. It is relatively easy to learn and keeps a record of all changes. Being able to code html, css and more you definitely need time and practice. I am currently programming a second website to get to know all the commands and layout options. Until that website is set up I will upload all my content here. GitHub and Gitlab are web-based hosting services for version control using Git, in our case we are using Gitlab.

My website runs with [Jekyll](https://jekyllrb.com/), a static website generator. The advantage of using Jekyll is that I can run my Website locally, and with that I am able to see the changes in realtime. For all my basic commands I use [Hyper](https://hyper.is/), which is an electron based terminal. The text is written in [Typora](https://typora.io/), a markdown editor. Markdown is a lightweight markup language which helps to make writing in code in for example Atom easier. Everything comes together in [Atom](https://atom.io/). Atom basically displays the website structure. Within Atom all changes to the content and layout can be made, and pushed. This workflow makes it easy for me to upload my content easy and fluid.

![]({{site.baseurl}}/images/fabacademy-pm-01.png)

I am running my site on a local server through the command *bundle exec jekyll serve* in Hyper. This allows me to see all changes made in instantly.

![]({{site.baseurl}}/images/fabacademy-pm-03.png)

Then I have added a *fabacademy html* to my layouts which I linked to my *reflection/ fabacademy.md* through adding *layout: fabacademy* to the header. This allows me to overwrite the reflections layout.

![]({{site.baseurl}}/images/fabacademy-pm-02.png)

To close the loop of the underlaying pages of fabacademy I added a fabacademy *output and permalink* to my *collections* in *_config.yml*. As well as adding *scope, path, type, values and layout* to the defaults within *_config.yml*.

![]({{site.baseurl}}/images/fabacademy-pm-04.png)

After completing this I added a *_fabacademy* folder which includes all the weekly assignments. I am basically using the same parameter as in my reflections.

![]({{site.baseurl}}/images/Websitechange.png)

The result is a fabacademy loop, which is called *how to make almost anything* within the reflection part.

## The commands
which I am using in Hyper (and Atom) to get into the folder/ change directory and run the local server are:
```C#
cd
```
To change directory
```C#
ls
```
To list computer files

```C#
bundle exec jekyll serve
```
And to run the local server
```C#
git clone
```
Cloning the Repository into local computer
```C#
git status
```
Staging the changed files
```C#
git commit -m
```
To write the message or comment on the staged file
```C#
git push
```  
Uploading/ pushing the commits
```C#
git fetch
```  
Fetching the branches from the Repository
```C#
git pull
```
Downloading the most recent changes made and merging them with the local copy
