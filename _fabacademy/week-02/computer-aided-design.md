---
title: Computer Aided Design
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-03.svg
published: true
---

### From a Sketch to a model

Before creating any object it easy to sketch out different designs and ideas. For that I usually prefer pen and paper, as it gives me the freedom to find a form before defining it. Several ideas can then be build in different CAD programmes. Building an object virtually will never replace the hands on experience of building, but it helps understanding proportions and basic technical limitations. There are several 2D and 3D programmes used in the design world, for example [Adobe Illustrator](https://www.adobe.com/products/illustrator.html), [Corel Draw](https://www.coreldraw.com/en/product/coreldraw/?sourceid=cdgs2018-xx-ppc_brkws&x-vehicle=ppc_brkws&gclid=Cj0KCQiA-onjBRDSARIsAEZXcKYPcoj7P6-705ZlXePDt7gYPsjG2_SMNiuXgRpOayXNI7JbQRNrVnMaAuYDEALw_wcB), [Inkscape](https://inkscape.org/), [Rhino](https://www.rhino3d.com/), [Fusion 360](https://www.autodesk.com/products/fusion-360/students-teachers-educators), [Maya](https://www.autodesk.com/products/maya/overview) and [Sketchup](https://www.sketchup.com/plans-and-pricing/sketchup-free) to name only a few. In the past years I've used Rhino for all my 3D models, and I feel pretty confident using it.

This weeks task it so create a parametric design. I've decided to learn a new program to broaden my expertise. Fusion 360 allows to to change the variables of the design easily. There are several easy to follow tutorials on the Web.


![]({{site.baseurl}}/images/190209_CAD01.png)

For starters I've created a small [sketch](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-02/190209_Sketch.ai.zip) in Illustrator of the modular system I want to build. For that I used the pen tool, lines and boxes. The sketch is in a 1:10 scale. Creating a 3D model from a sketch helps to see if the model will work in reality. The proportions and sizing can then be edited using a parametric design tool like [Grasshopper](https://www.grasshopper3d.com/).

![]({{site.baseurl}}/images/Fusion360_Tutorial.png)

![]({{site.baseurl}}/images/190209_CAD02.png)

I've tried out several tutorials to slowly get to know the program. It took very long to get used to the new interface, after using Rhino for so many years. The main difference is how to navigate in the space. Normally I would use my right mouse to position the view on the object. In Fusion everything is moved with something similar to 'gumball' in the top right corner. I feel less free by using that. The commands are not typed in usually like in Rhino, but by clicking right on the mouse a new menu open which gives more options of further steps.

![]({{site.baseurl}}/images/190209_CAD03.png)

The basic structure was build with simple commands like 'centre rectangle', '2 point rectangle', and extrude. I find the interface less then intuitive, and I still have to get used to the different layer system on the left hand side.

![]({{site.baseurl}}/images/190209_CAD04.png)

After trying out Fusion 360 for a few hours I have to admit that I like Rhino more. Fusion could be a good option for someone who has not used any CAD software before.Then all the commands will not feel that counter productive. For the future I will use Rhino again, and will try to get my hands on the new grasshopper.

![]({{site.baseurl}}/images/190211_Grasshopper.png)

I have started a grasshopper tutorial, to get to know the functions. I will have to work with this computer program during the weekend to get into the flow of using it. For next week assignment we will have to use a parametric design tool for laser-cutting. Therefore I want to get used to it quickly before I have to work with it more often.

A link to the files is [here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-02/Computeraideddesign.zip).
