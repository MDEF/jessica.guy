---
title: Application and Implication
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-17.svg
published: true
---
## What the heck am I doing?
I am trying to make an electronic sensing organ as elaborated in my [final project proposal](https://mdef.gitlab.io/jessica.guy/fabacademy/final-project/). This organ will translate data from the city into a synesthetic experience for humans. The goal is to make people aware of their environment, making visible the unseen connections. The project is an indirect approach of data representation instead of data visualisation. This will be an invitation to explore city dynamics and how citizens want to experience the city of the future. I have made a [Gantt diagram](https://www.gantt.com/) in [Google Sheets](https://docs.google.com/spreadsheets/d/11OB4e6DVy1Nut2DuW4skSnJuY2-JrA_c3ZfGGM2aUx4/edit?usp=sharing) for my master project which will include my final project from Fabacademy, as my physical object will be made during the Fabacademy classes.


### What will it do?
I will make an electronic device which will sense frequencies of the electromagnetic spectrum and translate it into a light/ sound/ vibration experience for humans. This electronic device, or organ, will be integrated in some kind of wearable to be able to live and experience the city long term.

### Who's done what beforehand?
Electronic sensing organs are being researched for a couple of years. The most famous example is Neil Harbisson, who uses a light sensor to translate color into sound. There are multiple approaches of translating experiences for people with disabilities. For example glasses from [AI serve](https://www.aiserve.co/) which gives hearable feedback about the environment for the visually impaired. There is an electronic patch for the tongue which translates taste into a vibration. And there is a [VEST](https://www.futurity.org/deaf-vest-vibration-senses-893352/)project which translate sound into a vibrating vest for death, to "feel" speech. Data visualisation is not something new. What is interesting about data representation is that this approach tries not only to make sense of data but makes it experienceable. None of these projects translate city data into a wearable experience device/ sensing organ to extend the human perception.

### What will you design?
I will design the translation between the data input and experience output. Further I will have to design the organ/ wearble itself. It will have to be durable and not to big in size. My goal is that this organ becomes part of ones daily life, therefore the design of interaction, and general design of it are very important.

### What materials and components will be used?
I will use an EMF sensor, based on this [project](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/) and translate the input into an output which can be experienced by humans. For example into light or vibration. I will design a wearable in the next week to see what kind of materials I will need for it. I am researching how I can make an easy to use, not too expensive, sensing device with one of our faculty members. I will also need an Arduino Uno board, Mosfets and Vibration motors for the output.

BOM
- 1x double sided copper PCB
- 2x Resistor 10K
- 1x Resistor 47R
- 3x Capacitor 1nF = 1000 pF
- 1x Capacitor 100nf = 10000pF
- 1x LT5534
- 1x Arduino Uno
- 3x Mosfets IRF Z44N
- 3x LRA Vibration Motors
- Mulitple wires, solder, solder iron, shrinking tubing etc.

To meassure EMF with these antenna wire loop lengths:
- 2.4 GHz = 12.5 cm  / Bluetooth, WLAN
- 1800MHz = 16 cm / E Netz GSM
- 900MHz = 33 cm / D Netz GSM
- 500 MHz = 60 cm / DVBT K24 Television
- 100 MHz = 150 cm / FM Broadcast

### Where will it come from?
We have ordered the LT5534 from [Digikey](https://www.digikey.com/). The rest of the components, at least for the first sensors, will come from the FabLab. I will have to see what kind of materials will be used for the wearable.

### How much will they cost?
For the electronics I will need to spend around 20-30 € in total, depending on the vendor. The most expensive part is the Microchip for the EMF detector, which is about 10€. The Solder Iron, solder, shrinking tubing and wires are not included in that price. If you would include electricity used and the 3D print of the structure for the electronics the project will overall cost about 60€ for each wearable/ electronic organ which I am making. When you would produce more than one/ make them in larger amounts this amount would way be less. 

### What parts and systems will be made?
I will probably make a circuit board to connect the sensor and output components. A shell or similar has to be fabricated. The shell has to keep the electronic components in place, but will have to be accessible to be able to exchange or update parts. The design of the device is very important as it should be something I/ other people want to live with longterm.

### What processes will be used?
I will use Eagle for the design of the PCB and the CNC milling machine for making the circuitboard. The shell will probably be 3D printed or I will make something through molding and casting. Depending on the material properties I will need.  

### What questions need to be answered?
How and with what am I going to sense a range of frequencies of the electromagnetic spectrum?
Will it need some kind of battery for charging?
How long will the battery last? How often will does it have to be recharged?
How big will the sensor be?
How will the organ/ wearable look like?
Can it be a combination of low/ high-tech?
How am I going to live with it?
What will be the experience during living with it?
Can the design/ making of it be distributed with a toolkit?
Does there have to an online platform or similar to share the experiences?

### How will it be evaluated?
I will let other people live with the organ, the feedback from them will help me evaluate my project.
