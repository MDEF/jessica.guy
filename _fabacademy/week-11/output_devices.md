---
title: Output devices
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-12.svg
published: true
---

## Notes from the class
In the end Outputs are the opposites of Inputs. We have information which we want to translate into an action. One of the most used ways of generating an output is through magnetism. With coils we can control motors and speakers etc. These are some examples:

**DC Motors** is a motor which contains main parts. One static and one movable part. "A circular permanent magnet was fixed inside dc motor, and the current through the coil force on the rotor produces ampere, When the coil on the rotor is parallel to the magnetic field of the circular permanent magnet, affected by the magnetic field direction will change. Because the brush at the end of the rotor alternates with the converter , the direction of the electric current on the coil change, and the direction of the lorentz force is constant, so the motor can keep rotating in a direction."

**Servo Motors**  are controlled by sending them a pulse of variable width. The control wire is used to send this pulse. The parameters for this pulse are that it has a minimum pulse, a maximum pulse, and a repetition rate. Given the rotation constraints of the servo, neutral is defined to be the position where the servo has exactly the same amount of potential rotation in the clockwise direction as it does in the counter clockwise direction.

**Stepper Motors** is a brushless electromechanical device which converts the train of electric pulses applied at their excitation windings into precisely defined step-by-step mechanical shaft rotation. The shaft of the motor rotates through a fixed angle for each discrete pulse. This rotation can be linear or angular.It gets one step movement for a single pulse input.

**Solanoids** A solenoid is a coil of wire in a corkscrew shape wrapped around a piston, often made of iron. As in all electromagnets, a magnetic field is created when an electric current passes through the wire.

<https://learn.sparkfun.com/tutorials/motors-and-selecting-the-right-one/dc-brush-motors---the-classic>

**PWM Pulse Width Modulation** Pulse width modulation (PWM) is a powerful technique for controlling analog circuits with a microprocessor's digital outputs. PWM is employed in a wide variety of applications, ranging from measurement and communications to power control and conversion.

**LED Light Emitting Diodes** can only use a certain amount of ampere we can use the OHM law to calculate the right Resistor to make sure we do not burn the LED. LEDs are like tiny lightbulbs. However, LEDs require a lot less power to light up by comparison. They're also more energy efficient, so they don't tend to get hot like conventional lightbulbs do.

**WS2812** is an intelligent control LED light source that the control circuit and RGB chip are integrated in. a package of 5050 components. It internal include intelligent digital port data latch and signal reshaping amplification drive circuit. Use *Fast LED* Library from Adafruits.

## Making an output
This weeks assignment is to make an output. For that we can use an existing PCB design and mill it ourselves. I have decided to make an Fabduino board as I will need it for my electronic sensing organ in my master thesis. The output will be multiple LRA vibration motors. LRA's are Linear Resonant Actuators and are comparing them to Eccentric Rotating Mass vibration motor (ERM) more sufficient for my usage. The are small and round and work very well in a "wearable".

I will use the LRA's motors in combination with Metal Oxide Semiconductor Field Effect Transistor or short MOSFET. The MOSFETs are transistors which work as a semiconductor device. It is like a switch for energy or amplifies electronic signals in electronic devices. The LRA's work both with 3.3 V and 5 V therefor I will not need an extra converter. Also the MOSFET'S will take over this part.

I used the design files from the FabAcademy [embedded programming repo](http://academy.cba.mit.edu/classes/embedded_programming/).

![]({{site.baseurl}}/images/Fabduino.png)

![]({{site.baseurl}}/images/Fabduino_Components.png)

The Components I will need are:
- 1x ATMEGA28P or ATMEGA168
- 1x 1 uF Capacitor
- 1x 0.1 uF Capacitor
- 1x 10k Resistor
- 1x 1k Resistor
- 1x 0 Resistor/ Jumper
- 2x ISP connector
- 8 MHz Resonator


![]({{site.baseurl}}/images/Output01.jpg)

First you will have to upload the PNG of the PCB deign to fabmodules. Make sure to set the ouput to the CNC milling machine and the end mill which you are using. For the traces you will use the 1/64 end mill and for the outline the 1/32 end mill.

![]({{site.baseurl}}/images/Output02.jpg)

Then let the software calculate the design, check if it actually looks the way you want to and that all traces are also being cut. I used an offset of 4 with 50% overlap. The .rml file will be saved to the download folder.

![]({{site.baseurl}}/images/Output03.jpg)

Then you will have to set your X,Y and Z. Put in you end mill and move the head to the corner of your copper PCB. First set X and Y and push the X/Y button in the software. It will ask you if you want to set the origin, click YES. Then move down the end mill with the software until it almost touches the copper. Then unfasten the end mill and gently set it on the surface of the copper. Fasten the end mill again and set the origin in the software on the computer.

![]({{site.baseurl}}/images/Output04.jpg)

The next step is to load the file. Puch the button *cut*, then press *delete all* to delete old files. With *add* you can choose your file and then press *output*. The machine will automatically start milling so be sure to set you X,Y and Z beforehand.  

![]({{site.baseurl}}/images/Output05.jpg)

Then do the same again for the outline/ interior file. But make sure to change your end mill to 1/32 and set the Z origin again. Be careful not to mess up your X and Y origin, otherwise the outline/ cut will not be aligned with the traces of your PCB.

![]({{site.baseurl}}/images/Output06.jpg)

Et Voilá! Your fabduino PCB is done. Now off to soldering, the most fun part of all. 
