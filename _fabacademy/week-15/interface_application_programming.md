---
title: Interface and Application Programming
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-15.svg
published: true
---
![]({{site.baseurl}}/images/Interface00.jpg)

This weeks assignment is to use an Input or Output and make an interface for it. I have decided to make an interface for a soil humidity sensor. For that I will need:

- raspberry Pi + SD card
- Capacitive Soil Moisture Sensor
- ESP8866 NodeMCU Board

### Setting up the RaspberryPi and IP scanning

First add SD card from raspberry pi to Computer and make a ssh file without anything inside

![]({{site.baseurl}}/images/Interface03.png)

Then make a *wpa_supplicant-conf* file and add this code to it

```C#
update_config=1
country=US

network={
     ssid="Your network name/SSID"
     psk="Your WPA/WPA2 security key"
     key_mgmt=WPA-PSK
}
```
![]({{site.baseurl}}/images/Interface01.png)

Change the *ssid* and *psk* to your *Wifi network name* and *password*

![]({{site.baseurl}}/images/Interface02.png)

Add both of the files to the Raspberry Pi SD card. The RaspberryPi is looking for the file which is called SSH, and if it finds it will enables is. The SSH is a protocol which function is to remote access the Pi

Download a IP scanner which is compatible with your Operating system. I have chosen [Angry Ip scanner](https://angryip.org/download/#mac) for Mac

![]({{site.baseurl}}/images/Interface04.png)

When using a Mac OS you can use the terminal to connect to a SSH, this is the [tutorial](https://mediatemple.net/community/products/dv/204405144/using-ssh-in-terminal.app-(mac-os-x)) I have followed.

Then connect the RaspberryPi to your computer. This will take about a minute until it boots.

For some reason I was not able to open the Angry IP Scanner. Even though I have opened the *System Preferences* and *Secrurity* options and allowed the program to be opened (Even though its from an unknown developer) it did not work. So after googling the problem and not finding the right solution I have decided to download another program which is called [IPscanner](https://itunes.apple.com/md/app/ip-scanner/id404167149?mt=12).

Scan the networks with your IP scanner for the IP of the RaspberryPi.

![]({{site.baseurl}}/images/Interface06.png)

To connect to the Pi by ssh open Terminal. Enter "ssh pi@192.168.1.188" and enter the password from the Pi user.
Do not forget to add the *pi@* otherwise you enter as not existing or computer user and the access will be denied.

![]({{site.baseurl}}/images/Interface07.png)

### Mosquitto and Node-red

First update the Pi, for that you enter *ifconfig* to enter the Pi and then *sudo apt-et update* to update. And then *sudo apt install mosquitto* to Mqtt mosquitto server.

![]({{site.baseurl}}/images/Interface08.png)

![]({{site.baseurl}}/images/Interface09.png)

Then I have installed [node red](https://nodered.org/) which is a flow based development tool for visual programming. With this I will be able to visualise the data from the moisture sensor. To install the node red I have used [this code](https://nodered.org/docs/hardware/raspberrypi) and entered it in the terminal. This will take a few minutes.

![]({{site.baseurl}}/images/Interface10.png)

After node-red is installed it will give you a local IP address. I went into my browser and added the IP address of the RaspberryPi + the last 4 digits of the IP address of the node; Which looks like this then: 192.168.1.188:1880 which will access the node red. To acces the UI you will have to write 192.168.1.188:1880/ui into the browser.

![]({{site.baseurl}}/images/Interface11.png)

![]({{site.baseurl}}/images/Interface12.png)


### Programming the MCU

To start programming the NodeMCU I have used this documentation with the [NodeMCU driver for Mac](https://cityos-air.readme.io/docs/1-mac-os-usb-drivers-for-nodemcu).

Install NodeMCU to boardsmanager [with thin link](https://dzone.com/articles/programming-the-esp8266-with-the-arduino-ide-in-3) in Arduino.

![]({{site.baseurl}}/images/Interface13.png)

I have used this [tutorial](https://www.hackster.io/amruthp/soil-moisture-sensor-arduino-tutorial-ffcb5b) to connect the moisture sensor to the NodeMCU Board.

![]({{site.baseurl}}/images/Interface05.jpg)

Then I have added this code which will read the humidity values:

```C#
int soilhumidity = 0;
int sensorpin = A0;

void setup() {
Serial.begin (9600);
}

void loop() {
soilhumidity = analogRead  (sensorpin);
Serial.println  (soilhumidity);
delay (100);
}

}
```

The first values I have received ranged between 2/3 to random numbers when I touched the sensor.

![]({{site.baseurl}}/images/Interface14.png)

So I have added *map* to transalte the reading into a percantage which can be shown in the UI.

```C#
int soilhumidity = 0;
int humiditypercentage = 0;
int sensorpin = A0;

void setup() {
  // put your setup code here, to run once:

Serial.begin (9600);

}

void loop() {
  // put your main code here, to run repeatedly:

soilhumidity = analogRead (sensorpin);
//Serial.println  (soilhumidity);
//delay (100);

humiditypercentage = map(soilhumidity, 760, 270, 0, 100);
Serial.println (humiditypercentage);
delay (100);

}
```

![]({{site.baseurl}}/images/Interface15.png)

![]({{site.baseurl}}/images/Interface16.png)
