---
title: Final Project
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-01.svg
published: true
---

### Project Description

For my Fab academy project I would like to work on Inputs from our Environment which then are being translated into outputs which can be read by the human interface. For this I will explore connections between us in the city and how I could make these experienceable.

Every day of our life is basically the same: waking up, eating, working, walking and sleeping. But the questions is: Are we really present? We evolved to such passive consumers of our life. Information which we can gain from our environment is being ignored. They are being displayed in 1 and 0 but what does these numbers mean? We rather glare in our phone, our gaze is occupied. Information about temperature, air quality, CO2 etc. will not enter our field of view. We don't "see" and therefor we don't understand, and we don't change. I would like to explore a new way of connection to our cities information.

There used to be, and there are still people who are able to smell if it is going to rain today. The change in the weather is no mystery them, like to us. This connection to our environment is something very precious. How could we connect to the city for a greater understanding of what is happening within? Two topics which have influenced me the most during the past weeks are Synethesia and Tacit knowledge.

### What is Tacit knowledge?
It is knowledge which is difficult to describe or teach to someone else. Explicit knowledge in comparison can be taught through verbalising or writing it down. Tacit knowledge is for example riding a bike or knowing when the clay for the pottery is just right. I would like to make this invisible knowledge tangible and experienceable collectively. To get to know our environment and connect to it. Just as Rafael Lozano-Hemmer does I would like to engage people to talk about contemporary problems in an artistic manner by creating an empathic link.

### What is Synesthesia?
The experience of two senses triggered simultaneously which will compliment each other. For example the ability to hear colour or see shapes to particular numbers. The project should be an experimental exploration of creating a new organ to sense and discover connections and adding a new layer of information to our environmental inputs. The goal is to understand us, our surrounding nature in a new way. This could lead to creating a new set of language. Two amazing examples for that are Neil Harbisson and Moon Ribas.

What does that mean for the next weeks for my project? I will try to make myself to an active receiver of connections to be able to reflect what is happening in my environment.

During Fab Academy I will use Inputs and Outputs to explore the ability to smell water quality, hear air pollution, listen to the sound of the colours of travel path, experience the surrounding of a synesthesia companion etc.
