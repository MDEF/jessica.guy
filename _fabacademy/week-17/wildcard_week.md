---
title: Wildcard Week
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-18.svg
published: true
---

For the Wildcard week we had three different tasks to choose from. We could either learn how to weld, how to bend wood (for example to make skateboards) or learn how to use the Robot to print clay. I have decided to be part of the welding class, as this is the most interesting for my Aeroponics project which I am working on.

## What is welding?
Welding is the process of melting/ joining two materials together. The roots of welding can be found during the iron ages in Europe and Middle east. The first usage of electrical impulses for welding were in the 19th century, which shows the rich history and heritage of welding.

There are different kinds of welding processes: Arc welding, Gas and resistance. We have learned arc or MIG (Metal Inert Gas welding) welding, which uses something called a MIG gun to fuse two metals (sometimes thermoplastics) together. The MIG gun is connected to a generator which charges the tip electrically (its charged positive). A wire is feed through a tube to the tip of the gun which serves as the material for the welding. The second essential part of the welding is the clip or "earth clamp" (which is negatively charged/ the ground).


![]({{site.baseurl}}/images/Welding01.jpg)

## Safety first

Before starting to learn how to weld we have to know everything about the security during process of welding! First of all we are ALWAYS! wearing a protective mask. The mask will ensure that you will not blind yourself when welding. We have two different kind of masks in the lab. Two which have batteries which allows the user to see through the dark glass even when not welding, and automatically gets darker and protects the eyes when welding. The others are always dark and can be used by the observers. We always have to close the red curtains around the welding station. This will make sure that no innocent bystander will have negative side effects (getting blind) of the welding. Welding is very harmful for the eyes, ALWAYS make sure that everyone around you has protective gear or is outside of the red curtains. Besides the mask you will also need to wear gloves and a protective jacket. The jacket is non flammable and the gloves protect you from direct sparks etc.

![]({{site.baseurl}}/images/Welding02.jpg)

## Welding

There are two settings on the generator for the MIG gun. One is the speed with which the materials is pushed through the tube and the second is the voltage. Depending on your own speed/ ability while moving the MIG gun you will be able to set the material flow. Make sure that there is about 1 cm of material showing in the tip of the gun. The tip of the gun should never have left over welding material. If there is, it can be brushed off with a metal brush. The material should not have a bubble in the end, if there is just clip it off with pliers. The gas which is used in our lab is Corgon 15, which is a mixture of 85% Aragon and 15% CO2. Our setting were 2 - 2,5 speed for the electrode and 3 for the voltage. 

![]({{site.baseurl}}/images/Welding03.jpg)

We welded two pieces of steel which we cut off from a big piece of steel in the lab. The two pieces are then fastened on the table with a clamp. On the clamp we have clipped on the earth clamp. After making sure that the material or electrode has the right length and everyone wears their protective gear you are ready to weld! The first exercise is to weld a straight line to fuse the two parts together. I have tried to have a steady hand and follow the line between the two pieces, keeping a close distance to the piece. When holding the gun the electrode builds up a red hot bubble. The goal is to move this hot glowing bubble slowly across the surface. Small rotating movements will help to distribute the material evenly. In case you are hearing and seeing sparks, you are probably to far away, or the settings of the machine are not correct. The best outcome is an even and straight line which covers the gap of the two steel pieces.

It makes no sense to go across the gap again after welding if you are not happy with the result. The second layer will leave a porous space in between and will most likely break there. Rather file it down and do the whole process from start (Or do it right in the first place).

Be careful after the welding as the metal will be HOT! After the welding the piece is going be ground with a circular metal grinder. While doing that make sure to wear protective glasses for your eyes. Helpful are also gloves as the edges of the metal may be sharp and the metal hot. Grind the piece until the whole surface is smooth and shiny! After grinding your welding abilities will come to the surface. If you have welded well the gap will be closed, and will not show any irregularities.


![]({{site.baseurl}}/images/Welding06.jpg)
