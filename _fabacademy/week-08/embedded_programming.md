---
title: Embedded Programming
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-09.svg
published: true
---

C is a general computer language which gives the possibility to program machines instructions or in our case circuitboards. Arduino, which we have previously used during our masters, also uses C code. Learning C is like learning a language. Therefor programs like Arduino uses a lot of libraries which will make it easier for the user to code.

### Memory
There are two types of memory in the Attiny Microchip, dynamic memory and static memory. The lifetime and location of dynamic memory can be controlled exactly. Dynamic memory (DRAM - dynamic RAM) can only be used during runtime. Information can be accessed also when there is not power supply as it is placed with resistors and capacitors. To make sure that the source code is not extremely large, built in functions are compiled in Runtime libraries. Static memory like RAM (Random access memory) or SRAM (static RAM) gives access to information as long as there is a power supply. SRAM is faster and is used e.g. in a computers cache memory (CPU).

- DDRxn registers configures pins as input or output
- PINxn writes data to pins
- Portxn reads the data from the pins

The main difference between analog and digital is that digital can only be on or off. We can control the timing and modify it. In analog we can only use in a range. Digital signals are more precise but with analog we are able to make better fadings. Just like in digital and analog music ;). We can use sampling time to translate an analog signal to digital. The more samples used the more precise we can estimate the signal. It is possible to change the pins characteristics in the code (as long as it is not predetermined). For example using it first as an output and then as an input, and vice versa.  

For example the difference between Arduino and C code when setting up a pin is *pinMode (6,OUTPUT)*; is the same as DDRA = 0b01000000; C code gives you the opportunity to not only set one pin, but defines also HIGH or LOW for the other pins. Although C code is always in binary language we can use bit masking to make it more easy to program with it. It will mask a value, for example: 1<<6 = 0b0100000. In the end HEX files are the ones which are going to be send to the micro controller. Using the HEX code makes the board in its cycles much faster. But HEX is also more difficult to code. To make the microprocessor understand the code send the first which has to be done is to use a bootloader.

## Programming Assignment

First I had to troubleshoot my Hello board, it took me a few tries and advice of an expert to find what was not working. When I was connecting it to my computer it did not show up. The problem was that the capacitor between VCC and ground was to far away from the Attiny. After moving it nearer I also reheated all the connection to make sure that the solder is smooth. When using a flexible PCB it can always happen that the multimeter will confirm connections between the parts even if they are not connected. Because through the flexibility of the base the solder can crack, therefor the solder is not actually touching the copper nor the component.

![]({{site.baseurl}}/images/Programming00.png)

First of all I had to download the Attiny library and add it so my local Arduino libraries.

![]({{site.baseurl}}/images/Programming02.png)

I connected my Hello board with my Fab ISP to my computer. The Hello board is also connected to the computer through a six pin cable. Then I opened Arduino and the "blink" Sketch. In *Tools* - *Board* and *Processor* I have chosen the *Attiny 44*.

![]({{site.baseurl}}/images/Programming03.png)

The crystal I have added on my Hello board is  20Mhz crystal, therefor the *Clock* also has to be *External 20Mhz*.

![]({{site.baseurl}}/images/Programming04.png)

As programmer I have chosen the *USBtinyISP*. If I wouldn't have my own Fab ISP I could have used a Arduino Uno as a programmer. Then I started the *Burn Bootloader*.

![]({{site.baseurl}}/images/Programming05.png)

The burn of the bootloader worked without any problems. After sending the blink code the LED lid up, that was so satisfying!

![]({{site.baseurl}}/images/Programming06.png)

The next step was to change the code so that the LED and the Button is connected. And voilà! In the first try I used the AVRISP from the Lab, because I wanted to make sure that there is only one thing I have to worry about not working. In the second try though I used my own Fab ISP which worked out well.

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-08/ArduinoCode_Blink.zip) a link to the code

![]({{site.baseurl}}/images/Programming07.jpg)
