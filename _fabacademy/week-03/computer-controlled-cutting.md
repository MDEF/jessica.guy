---
title: Computer Controlled Cutting
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-04.svg
published: true
---

## Cutting Edge
During week three of Fab Academy we got an introduction in laser-cutting. For the weekly assignment it was necessary to work in a group to explore materials and laser-cut settings. The second assignment was to make a parametric design which involves a press fit structure. We are free to make which ever object we want, but the assignment can also be used to for the final project. I am currently not 100% percent sure what my final project for Fab Academy is going to be. I am thinking of making a wearable which combines nature and tech on human skin. Therefore I've decided to play with naturals shapes which could be used in an architectural environment.

My inspiration comes from nature and architecture. Observing combinations of elements in nature and translating that into an easy shape, which then can be laser-cut.

![]({{site.baseurl}}/images/190212_DesignInspo.png)

After examining the shapes, and having the time frame in mind, I've decided on an easy rectangular shape. The [rectangle](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-03/190211_Lasercutting.3dm.zip) gives me the many assembly options. I've added two cut outs on each side of the rectangle so combine them. Another two in the middle of the rectangle can be used for structural improvement purposes. This design should give the overall structure enough stability.

![]({{site.baseurl}}/images/190212_Lasercutting01.png)

I've decided to use small measurements at first to make sure that I am not wasting to much material for the first try-out. The cut outs in the rectangle are 2.2 mm wide, which is the width of the cardboard which I am going to us for the mockup. The overall size of each piece is 400 x 300 mm. The design can then be edited in Grasshopper to change the parameters. I've cut out four boards at first, with two different cut out sizes, to make sure that all the measurements are correct, and that the structure stable.

![]({{site.baseurl}}/images/Lasercutview.jpg)

![]({{site.baseurl}}/images/Lasercutexample01.jpg)

![]({{site.baseurl}}/images/Lasercutexample02.jpg)


## Group Assignment

For the Group assignment we created three different files. Each files has been created in a different program. This allows us to observe which is the easiest to use for future projects. The test for the engraving and rastering has been made in [Adobe Illustrator](https://www.adobe.com/products/illustrator.html), the [Kerf](https://www.instructables.com/id/How-to-Adjust-for-Wood-Thickness-and-Kerf-on-a-Las/) test in [Rhino](https://www.rhino3d.com/) and the press fit test in [Autocad](https://www.autodesk.com/products/autocad/overview). There are three important which have to be taking into account before cutting anything with laser. First the laser-beam has to be focused on the material, which can be done with a little tool. Second the ventilation has to be switched on, otherwise toxic gasses can escape. And third never leave the laser-cutter alone! Just in case it sets on fire.  

### Engraving and rastering

Laser engraving and rastering is often used for various creative objects. I have used both for my design dialogues presentation before. I found out that some shapes come out clearer then others. This partially depends on the material itself, but also on the setting. For this reason I've created three little circular design to try out different raster setting. The squares are the basic test which can bee seen on previous Fab Academy assignments. All of the objects are being cut with different setting and afterwords the differences can be examined. Also we have chosen another image of a friend of ours, which we have edited in illustrator. With this one we have chosen wood as the material for laser-cutting, which worked out very well.

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-03/190211_LasercuttingGroup%20Assignment.ai.zip) a link to the files


![]({{site.baseurl}}/images/190212_LasercutterTest01.png)

![]({{site.baseurl}}/images/ryota.png)

![]({{site.baseurl}}/images/ryotatest2.jpg)

### Kerf test

To use the laser cutter to its full potential one has to understand the all the parameters of it. For example part of the material which is being cut will burn away, which is called kerf. The laser kerf is determined by the material properties, cutting power, cutting speed and  the focal length of the laser. This tolerance has to be taken into account during the design process. An easy test to try this out is the [Kerf Test](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-03/190212_Kerf.3dm.zip). For that a simple 5 x 10 cm square with 1 cm segments can be cut out and then measured. The result will show that after cutting the material a certain percentage will have been burned away. This makes the outcome smaller than the original design.

![]({{site.baseurl}}/images/190212_LasercutterTest02.png)

![]({{site.baseurl}}/images/Kerftest01.jpg)

We wanted the first test to be on one of the left over cardboards. Unfortunately we have not positioned the file correctly, therefor it collided with one of the existing cutouts. Nevertheless we where able to measure the difference between the original file and cut one.

### Press fit test

We designed two elements with different shaped cut outs to try out the press fit test. The goal of the test is to find out which slot size is correct according to the material thickness. After assembling the pieces an element with good structural properties can be achieved. Press fit is an easy way of making the joints for a cardboard, plywood or acrylic construction.

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-03/190213_Pressfittest.zip) a link to the file

![]({{site.baseurl}}/images/Pressfit.jpeg)

We have not added a margin, because after burning away part of the material through the material the two pieces will fit together perfectly.

![]({{site.baseurl}}/images/Pressfittest01.jpg)

### Vinyl cutting

The vinyl-cutter is fairly easy to use in comparison to the laser-cutter. Firstly I created a design, in my case I used Illustrator. But you can use any CAD software, the only requirements is to save it as a .dxf file. I made an "all seeing eye" sticker, which I can add to my collection on my Macbook. I wanted to make something of use, and for me stickers are something which can not only personalise an object but also reminds you of something precious.

![]({{site.baseurl}}/images/Vinylcutview.jpg)

The material is being fastened underneath the two rolls. After that the button 'feed' on the machine, called [Cameo](http://www.silhouettecameoeurope.com/vinyl-cutter), has to be pressed. The machine then automatically feeds the material to the correct position. The file has to be opened with the computer software 'silhouette', which comes with the vinyl-cutter. After that the steps are pretty easy, just adjust the design to the size of the material, choose the right material in the setting and hit print.

![]({{site.baseurl}}/images/Vinycuttest.jpg)

I've cut the the Eyes two times, in two different sizes. In the first try I made it very small, to see how precise the machine can work. Additionally it saves material in case of it going wrong in the first time. Thankfully everything worked out very well. I am happy with the result and will definitely use it again. I am curious to experiment with it for next week assignment, cutting copper circuits.

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-03/190213_Vinylcutter.ai.zip) a link to the vinyl cutting files
