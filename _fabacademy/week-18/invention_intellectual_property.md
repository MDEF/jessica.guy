---
title: Invention, Intellectual Property and Income
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-19.svg
published: true
---
One thing which always has to be taken into account when brining your project to the real world is  how to secure intellectual property and preferably generate money from it. During the last trimester we had one very important class which encouraged us to think about this, which was “Emergent Business Models” led by Valeria Righi and Javier Creus from Ideas for Change, which fits perfectly into this weeks Fab Academy topic.

This weeks assignment was develop a plan for dissemination of your final project.

## Licences, Patent, Copy right
There are multiple kind of licences ranging from legally govern use and distribution to freedom to use and redistributed. On the other hand we have the Patent which protects your idea or design from being distributed, miss-used, made or used for making money from other people for a certain amount of years. The procedure to get a patent involves multiple steps and has to be paid for. For example many patents are further developments from older inventions. An agreement has to be set between the old and new patent holder, when the older patent is going to be part of the patent. The industry uses patents to ensure they are having an advantage to their competitor. This makes problems for example in the pharmaceutical industry. Where patents are being used by big companies, which are then selling expensive medicine to gain more money. Without any competitor cheap and life saving medicine is often out of range for people with a low income. All rights reserved or "copy right" means that the owner reserves, or holds the use for this invention or design. When it expires it automatically turns into a public domain. Further examples are explained in the [Fabacdemy documentation](http://fabacademy.org/2019/labs/barcelona/local/wa/week18/)

## Open Source
After following the [Creative Commons Diagram](http://creativecommons.org.au/content/licensing-flowchart.pdf) I have decided that my project should run under the Creative Commons Attribution [Share Alike licence](https://creativecommons.org/licenses/by-sa/3.0/) This means that anyone can *Share — copy and redistribute the material in any medium or format* and *Adapt — remix, transform, and build upon the material for any purpose, even commercially.*

## Project plans
After developing one kind of organ, which translates electromagnetic frequencies into vibrations, I would like to explore further ways of sensing environmental influences.

### Making
To make this project happen I will make more wearable sensor organs. This could be mainly developed in the Fab Lab as I would need either the vinyl cutter or the small CNC machine for cutting the PCB’s. Nevertheless it is also possible to set up a mini Fab Lab in my own apartment or studio. All I would need for that is a soldering station, vinyl cutter, my computer and some space. Which is fairly easy and would cost around 200-300 € to set up.

One topic which I have not been taking into account until now is the materials I have been using. This idea has been opened up at an exhibition I went to with my Aeroponics project. We have discussed with the visitors of the exhibition resource consumption of Aeroponics. The system could gain its electricity from sustainable resources and uses less water and pesticides, which makes it particularly advantageous. But what I have not been taking into account so far, is the production line of the electronics and computer which are being used.

This influences the way I am looking at the materials I am using for my organ. The European Commission customised the implementation of the “Circular Economy Action Plan” in 2019. This plan sketches out the challenges which our society is going to face in the near future. It explains a set of strategies which have to be implemented for the European Union. If we are talking about a sustainability model, the word sustainability implies that my project should fit in a circular economy. To go further from producing, consuming and wasting. The material flow, resources and recycling aspect are very important. Since the organ senses invisible data about environmental influences, the organ itself should not have a negative impact on our environment. One idea would be to take recycling of old electronics into consideration.

### Distribution
I am considering multiple layers and steps for the distribution of my project. The first step is our exhibition in June in which we will present our master projects. This first exhibition will give me feedback and information about what has to be improved. The second step is to make an online platform or similar. This could either be part of my personal portfolio/ website or an independent one. Then I will apply for the Dutch Design Week as well as the Biotopia festival next year, which has the topic “Senses”. Visibility for my project can also be gained through applying in residencies and other festivals/ exhibitions through out the year.

Collaborations with maker spaces, schools or projects like the “smart citizen kit” could engage local communities into making and learning about electronics and city dynamics. When working with other designers, makers and artist the project can be enriched and new points of views can give the project the necessary diversity. Altogether we can find new creative ways of seeing/ experiencing environmental issues and make new/ more sensors organs.  

I want to take into account different use case scenarios. For example for kids it might be more interested in having the sensor organ rather in their room or house than on their body. Placing it in the house could make the whole family engage with it. A sensor organ kit could have different configurations. For example developing one kind of organ - trying out different inputs and outputs to then mix and match. Depending on which age and interest the user has the kit could contain a ready made organs, a “half finished” one - to either make the electronic parts or the coding or one which has to be made from scratch. Overall I want to play with the positioning on the
body - home - or city/ community spaces. But in the end my ideal version is an electronics sensing organ which is part of your body and part of your daily life. And creating a network around data sensing, sharing knowledge about making and electronics.
