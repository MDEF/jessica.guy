---
title: Computer Controlled Machining
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-08.svg
published: true
---

CNC means Computer numerical control that means that there is CNC printing, CNC laser-CNC cutting, CNC milling etc. All of them are using a gCode, which gives information about what tools to use, turning on etc and the position and speed for cutting. The CNC milling machine in IAAC uses 3 axes to make 2D and 3D objects. It can be used for producing big and durable and strong parts. Working with one homogenous material beneficial because the properties of the material are the same throughout the whole piece. In comparison to 3D printing where the properties vary between the layers. You are able to cut almost anything with the CNC milling machine, aluminium, wood, plastic, foam etc. Always try to use the material to its full potential and take into account how much "waste" will be left over. Nesting is very important when using the CNC. Make sure that there is as little waste as possible by designing and arranging the pieces on the material favourable. This saves money and material.


Accuracy of a CNC machine is very good, because it is more consitent then moulds. The accuracy of a mould will change after using it for a long time. But there is friction and therefor the end mill has to exchanged after a while. For making a chair by hand you need at least 4 tools, but for making it with a CNC you will only need one CNC machine. The machine itself does not need a lot of maintenance nor needs a break.

There are Router and Spindle motors. The spindle is more expensive, better quality, quieter and the speed can be controlled by software. The Router is cheaper, louder and the speed can only be changed by hardware. The collet holds the end mill in place. Always check that the collet is clean when changing the end mill, to make sure that the end mill is secured safe. The collet always have to be the same size as the end mill.

### Know how

The CNC always rotate clockwise! They are made out of carbide or high speed steel.

- Straight flute: Plywood, MDF, Plastics
- Up cut flute: Metals, MDF, HDF (Hard materials), hard plastics (recommendation O flute mill)
- Down cut flute: Soft woods (Soft materials)
- Compression flute:

There are different shapes for the end mill *Flat end mill, Round end mill and Shape end mill*. A *Spoil Board cutter* Is for cutting the surface of a material, but cannot make any holes. The more flutes the end mill it has the more accurate it is. If you have 6 mm end mill I can only go down 3 mm into the material. Bigger is better, the bigger the more material you can cut at ones and the stronger is end mill itself.

Always make sure that the material is secured fast so it cannot be lifted by the end mill. If the the velocity is to fast the machine will "scream" for more 'chips' and speed. If the sound is low the speed is to high and the end mill might break soon.

Spindle speed is the rational speed of the cutting. Feed rate is the surface speed at the centre of the rotating tool. Step down and Step up is..

Chip load is the amount of material which you are cutting in inches per minute divided by the spindle x the number of flutes.

### How to secure your material
The best way is to use a vacuum table. But they need a lot of maintenance and only works for large parts. Clamps can be fixed on the table and will hold down your material. When using screws you have to make sure that the end mill never hit the screws. The screws are easy to use and fast. For foam it is the best to use a combination between double sided tape and clamping it on the side. Add a little tap which will not be cut will help to hold down the piece, make sure that the tap is connected to the main material. When nesting the design make sure to leave 10 mm plus the two times the diameter space between the pieces.

### Security
Always eye protection! Tie up your hair and clothing has to be tight on the body! Wear proper shoes no flipflops! Listen to the machine but don't put your head in there! Check all the variables! There is always only one operator on the machine! Always do a test cut in the air before!

## CNC milling Assignment

For the assignment I grouped up with [Saira](https://mdef.gitlab.io/saira.raza/) to make a bar for our classrooms balcony. We wanted it to fit perfectly into the slanted corner. We took inspiration from [Open desk](https://www.opendesk.cc/) and other open source furniture websites.

![]({{site.baseurl}}/images/Milling01.png)

We have started with making rectangles which with the dimensions of the space on the balcony which will give guidance for the design dimensions later. With the line tool we made the polygon shape according to our sketch. The outline can then be extruded to 15.2 mm (We have measured out piece of wood before and the average thickness of the plywood was 15.2 mm).

![]({{site.baseurl}}/images/Milling02.png)

Further more we added extra construction line to make the designing of the joint pieces easier. Besides having the two shelve surfaces and the two legs We added a structural joint in the middle. This will give the bar strength both in the X and Y axes.

![]({{site.baseurl}}/images/Milling03.png)

We added dog bone cut outs, after extruding all the pieces and using the boolean difference tool to make the cut outs. When we use a 6 mm end mill we will never be able to make a 90 degrees angle in inner corners, because of the circular shape of the end mill. For this reason we added dog bones in every corner. Especially in the ones where two pieces are fitted into each other.

![]({{site.baseurl}}/images/Milling05.png)

For nesting all the 2D pieces we added a rectangle with the size of our plywood sheet. We added an offset from 20 mm to make sure that not of the pieces will be to near to the corner. Also this gives us enough space to add engravings for the screws.

![]({{site.baseurl}}/images/Milling04.png)

After finishing the overall design we used 'make 2D' to get all the outlines from the extruded pieces, including the cut outs. For the next we had so switch computers to use [RhinoCam](https://mecsoft.com/rhinocam-software/). RhinoCam allows us to set up the different strageties for profiling, engraving and pocketing the material.

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-07/190310_MDEFBar.zip) a link to the lasercutter test and the Rhino file


### Setting up the RhinoCam file
- We made sure that the right machine was setup (CNC-BCN)
- Set up stock size to size of the plywood sheet

![]({{site.baseurl}}/images/Milling06.jpg)

First Step: Setting up the engraving for the screws
- *Machine Operations*, *two axes*, *engraving*
- *Select drive containment regions* and then we selected all the points for the screws and hit enter
- Select *Clearance Plane* to make sure the machine moves up in the Z axes when going to the next job. This is particularly important when doing 3D models.
- *Tool* We had to add the 6 mm end mill to the library with the help of our Fab Lab manager. If the feeds and speeds are saved in the tool you will not have to change it later in the settings
- *Cut Parameters* we selected *climb (down cut)*. In Cut depth control *Total cut depth* we put in the 15.2 mm of the material plus another 0.3 - 0.4 mm to make sure it will cut all the way through. In total we have decided for 15.5 mm which will in the end turn as a little bit to less in one corner of the material. In *Finish Depth* we put in 4 mm which is related to the diameter of the end mill. The end mill has a diameter of 6 mm and with every layer the machine goes it should only go about half of the size of the end mill.  
- *Entry Exit* if it possible use *NONE* in both settings. For the profiling we will have to make different settings here.
- *Feeds and Speeds* this will be the same as set in the tool itself.
- In *Sorting* we have chosen *minimum distance sort* which will make sure that the machine is using the fastest way between the cutting pieces
- Hit *generate*

Second Step: Setting up pocketing for the cut outs
- *Machine Operations*, *two axes*, *pocketing*
- *Select drive containment regions* and then we selected first only one of the cut outs for the pocketing
- *Clearance Plane* - same as before
- *Tool* - same as before
- *Cut Parameters* we selected *climb (down cut)* which is the direction of the spindle and ticked the box *use outside/ inside for closed curves* and then *Inside*
- *Entry Exit* - same as before
- *Feeds and Speeds* this will be the same as set in the tool itself.
- In *Sorting* - same as before
- Hit *generate*
- After that we selected the others parts which should be pocketed in machine operations and added to the selection.

Third Step: Setting up for profiling
- *Machine Operations*, *two axes*, *profiling*
- *Select drive containment regions* and then we selected first only one of the cut outs for the pocketing
- *Clearance Plane* - same as before
- *Tool* - same as before
- *Cut Parameters* we selected *climb (down cut)* which is the direction of the spindle and ticked the box *use outside/ inside for closed curves* and then *Outside* as we want the inside of our design.
- In *Advanced cut parameters* we can add bridges to the profile, this will make sure that the piece will stay in place after the profiling is done. This makes the whole process securer.
- *Entry Exit* If it is not possible to choose *NONE* then we are able to choose the entry of the tool the same line or with point of entry. For plastic for example it would be better to have a point of entry. For the wood we used on the same line.
- *Feeds and Speeds* this will be the same as set in the tool itself.
- In *Sorting* - same as before
- Hit *generate*

We exported two files, because otherwise we are not able to screw the board to the machine. That means the first file will be engraving and the second file is pocketing and profiling.  

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-07/190310_RhinoCam_MDEFBar.zip) a link to the RhinoCam file

### Setting up the Raptor

![]({{site.baseurl}}/images/Milling07.jpg)

First of all the zero axes of the machine in *Jog/ Setup* has to be set. For that we can move the machine manually to the X and Y zero of the board. For the Z axis we used the button. We moved the X and Y just slightly above of the button and then enabled the *custom*, *set Z axis*.

![]({{site.baseurl}}/images/Milling08.jpg)

After engraving the first file, the machines stoped, this gave us the opportunity to screw on the board. We used three screws for the long sides each, and one for the the short ones.

![]({{site.baseurl}}/images/Milling09.jpg)

In *Program* we opened our *.nc* file and made sure that there is no gcode showing up red nor that there is any anomaly in the preview.

![]({{site.baseurl}}/images/Milling10.jpg)

When standing near the machine we have to make sure not to be to close, not having any loose items hanging and wearing protective goggles. To be on the safe side it is better not the leave the CNC machine alone in case that there are any problems.

![]({{site.baseurl}}/images/Milling11.jpg)

Unfortunately we made a mistake when nesting the pieces. One of the cut outs got misplaced but we first saw it after the milling. In the end we decided send one of the legs again to the CNC machine. The sanded down all pieces with the rotary sander and the edges by hand with a 80 grain sandpaper.  

![]({{site.baseurl}}/images/Milling12.jpg)

![]({{site.baseurl}}/images/Milling13.jpg)

![]({{site.baseurl}}/images/Milling14.jpg)
