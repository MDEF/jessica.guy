---
title: Electronics Design
date: 2019-02-07 12:00:00
img: images/Fabacademy_Cover-07.svg
published: true
---

This weeks task is to use the last weeks FAB ISP to program a newly made PCB. The new PCB needs to be designed and needs at leat one output and one input (for example LED and Button)

### Know How

- *CURRENT* is the directed flow of charge through a conductor
- *VOLTAGE* is the force that generates the current
- *RESISTANCE* is an opposition to current that is provided by a material, component, or circuit

- *Crystal* Helps setting the velocity of the microchip. A quartz crystal is a very stable and accurate resonator
- *Resistor*  Passive two-terminal electrical component that implements electrical resistance as a circuit element
- *Capicitor* is a passive two-terminal electrical component used to store energy electrostatically in an electric field
- *Diode* is to allow an electric current to pass in one direction, while blocking it in the opposite direction
- *ATTiny* High-performance microchip controller

- *MISO* Master in slave out *MOSI* Master out and slave out
- *VCC* Voltage
- *SCK/ SCL* Clock
- *GND* Ground
- *RESET* A lock which prevents the micro-controller to be over programmed

*FTDI Cable* includes another circuits, which has to be taken into account, it is available with 3.3 volt or 5 volt. There are a lot of patents on this chip thats why you most likely need to download a driver.

When programming the Arduino make sure that you connect the right pin from the ATtiny into the code. The name of the pins are not the same as the one from the ATtiny. Thats because some pins can have multiple tasks.

*Pull up* (more common)/ *down* will make sure that the signal which is being received has no noise. It will make the signal is 0 when it is not used. Without this guide the pin will read random signals. Even if the signal is not 100% zero it will read it as a zero.
Check on the Data-sheet of the Microchip if there is a Pull up/ down resistor included into the chip which is being used. With the "make fuze" during programing of the PCB we can set up these settings to a certain extend.

*Sync and Async* (Learn this at Sparkfun.com)
When you want two chips to communicate with each other, you have to set certain mechanism so the informations arrives correctly and is read correctly.
Parallel vs. Serial way of sending information. The clock signal tells the chip in which time the information will be received/ read. The "master" is the one who generates the time, what ever is attached to it is the "slave"

*Asynchronous*
The information will simple be send but the receiving chip will not know when the data is send. That makes the chip slower.

*Ssynchronous*
Gives the concept of time, this is called the "Clock Signal". Then both of the chips know when information will be send, and when to expect information. This makes the process of sending and receiving a lot faster.

### Assignment

The first task for this week is to design the PCB and then to mill it. For designing the board I used the program [Eagle](https://www.autodesk.com/products/eagle/overview) from Autocad. I have downloaded the [Fab Academy Library](https://github.com/Academany/FabAcademany-Resources/blob/master/files/fab.lbr) from Autocad to use it in Eagle. The library has then to be set up in Eagle to be able to use it. I set up a new project through right clicking in the sidebar, I simply named it "week 6 assignment". The following steps where to open a "Shematic" which allows you to add components and connection between them.

![]({{site.baseurl}}/images/ElectronicDesign01.png)

The main commands I have used, which can be find in the menu on the left, where "add part", "move", "label", "name", "value", "delete".

![]({{site.baseurl}}/images/ElectronicDesign02.png)

After adding all the components and connections needed I opened the "Board Layout" with the button "generate/ switch to board". Then I worked in between these two interfaces to detangle the connection and arrange the layout. The goal is to have no tangled connections, and if there is no other possibility a 0ohm resistor can be added to jump over connections.

![]({{site.baseurl}}/images/ElectronicDesign03.png)

To export the file you first have to choose the correct layers. Firstly I have hidden all layers and then chosen only "top", "dimension", "tNames" to have the traces for the mill. This can then be exported as a PNG. The second file I have exported shows only the "dimension" layer, this is for cutting the outline of the PCB.

![]({{site.baseurl}}/images/ElectronicDesign04.png)

It is important to export the image with the "monochrome" box ticked and the resolution should be 1000 dpi.

![]({{site.baseurl}}/images/ElectronicDesign05.png)

After rearranging because I have forgot to add the FTDI I saved the traces and outlines and saved it to the IAAC Cloud. To mill the PCB I have followed the same steps as two weeks ago when we milled the FABisp, saving two files with each the outline and the interior traces. First uploading the png file, checking that the files has the correct dimensions, and setting all the variables according to the mill. The interior traces are being milled with the 1/64 mill with the speed of 4, and for the outlines I used the 1/32 end mill with speed 0.5.

![]({{site.baseurl}}/images/VinylPCB-test02.jpg)

The PCB I made was missing a few parts unfortunately. Therefor I added them in the schematic and redesigned the layout. I would be able to make a "frankenstein" out of this PCB, adding wires to make it work. But I took this opportunity to try something out which I wanted to do anyway: flexible PCB's.

[Here](https://gitlab.com/MDEF/jessica.guy/blob/master/_fabacademy/week-06/190304_HelloBoard.zip) a link to the Eagle file

![]({{site.baseurl}}/images/ElectronicDesign06.jpg)

To make a flexible PCB you first have to prepare the material. The base is a see-through flexible PVC (Polyvinyl chloride) sheet. A conductive adhesive tape, in my case copper foil, can then be pasted on the PVC sheet. When sticking on the copper foil you have to make sure that there are no bubbles or wrinkles in the material, otherwise the PCB will not turn out well. Then I ave made a couple of tests with the two different Vinyl cutters which we have in the Fablab. It turns out that the Camm-servo works way better then the Cameo. The tracers are being cut very nice and the corners of each piece are more precise. I have changed the trace thickness to 20 instead of 16, to make sure that the traces are not to thin. I converted the PNG to a JPG to be able to cut the files from Eagle. In the SOFTWARE I then opend the file, and right clicked on the PCB design to get the "image traces". Then I let the machine measure the size of my material and set the origin.

![]({{site.baseurl}}/images/ElectronicDesign07.jpg)

After a few tryouts the setting which worked out the best were 130 gm/cm and speed 12 mm/second . I made an extra board, to have one to practice the soldering. After cleaning up the board, using tweezers and a cutter to take out the extra foil carefully, I cut out the desired shape. The overall appearance is very pleasing, and I will try to experiment more with it in the next weeks.

![]({{site.baseurl}}/images/ElectronicDesign08.jpg)

For soldering I practiced on one of the failed boards. I taped the sheet to a piece of wood to make sure that the sheet is not moving all over the place while soldering. The trick is to be fast and precise while soldering. Otherwise the solder-iron will melt the PVC. I turned up the heat a little to make sure that the solder is melting quickly. I used an iron which has a nice thin top to be able to only touch the copper and not the PVC.

![]({{site.baseurl}}/images/ElectronicDesign09.jpg)

After all the effort of the last weeks a little exercise to loosen up the tension. Here a short video of the fabbercise, because Fabacademy is so much fun, and we really do not want to disappoint our tutors. A better one will be coming soon (hopefully).

<iframe src="https://player.vimeo.com/video/332087473" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/332087473">
