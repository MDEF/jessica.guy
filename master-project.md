---
layout: page
permalink: /master-project/
---

![]({{site.baseurl}}/images/Organ.jpg)


## Initial Question
How can a cognitive artefact interact with us humans to help finding our own nature?

## First thoughts and explorations
The future looks bright. Technology gets better with every breath we take. In the past years human-nature-technology interaction has been drastically influenced by technological revolution. But how can we design for a future in which the paradigm technology is changing and developing so fast?

Cyborgs are not science-fiction anymore. With the phone as an extension of ourselves we are constantly hocked up to the web. We are spending hours being online, in a world with no friction, designed with and for us. It is shifting our focus

Can we use this opportunity to redesign our habits, our influence on our surrounding nature and social interactions for a future in which technology and nature is balanced?

I want to explore the interactions between human - nature - technology by researching rituals and using a positive feedback loop to influence our decision making.

How can we make connections visible? How can we understand invisible information between humans, nature and technology? Is there an interspecies communication worth exploring?

We get up every day, eat, walk, cycle, work, run, eat again and fall asleep. All these things are happening every day. But are we really present? We evolved to such passive consumers of our life. Information which we can gain from our environment is being ignored. They are being displayed in 1 and 0 but what does these numbers mean? We rather glare in our phone, our gaze is occupied. Information about temperature, air quality, CO2 etc. will not enter our field of view. We don't "see" and therefor we don't understand, and we don't change. I would like to explore a new way of connection to our cities information.

There used to be, and there are still people who are able to smell if it is going to rain today. The change in the weather is no mystery them, like to us. This connection to our environment is something very precious. How could we connect to the city for a greater understanding of what is happening within? Two topics which have influenced me the most during the past weeks are Synethesia and Tacit knowledge.

### Further Questions
- What is the history of Language?
- How does nature communicate (Biosemiotics)?
- How can we mediate between technology, nature and humans?
- How does our perception and stability of self changes in the age of technology?
- How can I hack the human interface?
- How can I create/ design a new ritual with technology?
- how can we make connections visible/ sensible?
- How can I design a cognitive artefact which interacts with us?

The elaborated on these topics and tried to answer the question in my [final project diary](https://mdef.gitlab.io/jessica.guy/reflections/design-studio/)

## Outline
Drastic [climate change](https://climate.nasa.gov/evidence/) is happening all over the world and is effecting all inhabitants and the environment. Yet there are still discussion over if it is really happening and if we have a chance of solving it in the little time we have. We can slowly see changes in the government and behaviour of the dwellers. But the change and the actions have to accelerate. The connection between the impact from our environment on us vs. our impact on the environment are often unseen. The loss of this connection to our surroundings makes it difficult for a large amount of people to actually change. "Why should what I buy at the supermarket have an impact on someone living in India" or "Driving with the car is just way more convenient than taking the bike" are often heard phrases and excuses of the population. My goal is to show the connection between actions and its impact on our environment. There is over 2.5 quintillion bytes of data created every single day. This data is produced by us, but still we have difficulties to read it. The relation between the numbers and the impact of what is the cause behind it is still unseen. The air-pollution today is 6.41 pm2.5 or 34.8 pm10 micrometer per cubic meter. What do these numbers mean? How should we react? How can we influence it? Where is data coming from? Is it from here or from over there? We can use the data in a smart way as a collective good. And no one has as much information about the city as the city itself. We have to engage people to take part in shaping the future or the city. For example to encourages people to partake in Citizen Science. To generate a better understanding of the city dynamics, not only for the residents but also for new people coming to the city.

![]({{site.baseurl}}/images/Intersection.svg)

### This project is an indirect approach to data representation and goes beyond data-visualisation which will help us to understand and make sense of information from data sets.

When designing an synesthetic organ it is relevant how we design the interaction with it. Making something physically present but also making it matter in an emotional sense. The phenomenological aspect is embedded in the material and object itself. The artefact is in this setting not passive but an active participant. It gives the sense of belonging or togetherness and it works as a symbol to transfer meaning. The material object makes the conceptional idea experienceable.

### This project is an invitation into a conversation about perception, data and how we want the experience of city of tomorrow to be.

The explanation of the my keywords can be read on my [Project Diary](https://mdef.gitlab.io/jessica.guy/reflections/design-studio/) Synesthesia, Tacit Knowledge, Data, Perception etc.

## Area of interests for my Intervention
{% figure caption: " " caption%}
<iframe
src="https://embed.kumu.io/9aefe72e610dc164aae113a4379c67b2"
  width="660" height="380" frameborder="0"></iframe>
{% endfigure %}

This map includes the developed weak signals from the Atlas of Weak Signals class. For further explanation how they relate to my project please go to my [Atlas of Weak Signals](https://mdef.gitlab.io/jessica.guy/reflections/atlas-of-weak-signals/) page.


## Design Action
My goal for this master is to make an electronic sensing organ which can be used by dwellers of the city. I want this to be used longterm and the experience should be pleasure rather than a negative feedback. There is no point of making an object which will give people a negative feedback for their actions. Taking my past experiences into account people will just get rid of something which will make them feel bad. But an object which makes data experienceable in real time, will help to understand the city in a new way. The moment in which this happens is between data and information. One possible outcome is a toolkit which will guide the user into making their own organs. Interesting would be an online map of real time phenomenological experiences, to see where interesting data sensing stimulation happens in the city. Which would also mean that there is a meta data hub for environmental issues. The result is that we can gain tacit knowledge about the city dynamics and it will expand our view of reality.

Here a link to all my [References, readings & state of the art](https://mdef.gitlab.io/jessica.guy/reflections/library/) related to my project.

## The Organ

For the design of the artefact or electronic sensing organ, I have decided to divide between the working electronic prototype and a speculated concept design. This gave me the freedom to experiment and speculate more with the design without being constraint with the electronics.

To find the right design I have to explore body, material and context. And then revisit ideas over and over again, to be tested not only by me but also by others.

### Why EMF?
Looking at the electromagnetic spectrum it is clear that all frequencies, may it be light, radio, ultraviolet or gamma rays are surrounding us at all times. The only part which we are able to see is light, which we see as colour, and this a fraction of the spectrum.

*What else is surrounding us what we cannot see? And how does it influence us?*

EMF is typically categorized in two subdivisions: ionizig radiation and nonionizing radiation. Ultraviolet, X-ray and gamma rays are categorized as ionizing radiation and we should limit our exposure to it as it can mutate our DNA and can have undesirable effects on us. Visible light, radio and microwaves are nonionizing radiation. Considering modern technology, nonionizing radiation is hardly avoidable as part of our daily life. The influences of EMF have been researched the last 30 years but have been partially inconclusive. Nevertheless, there are restrictions for using a Microwave, almost all of us have this kitchen appliance at home, not many people question its influence. But one thing is for sure, if you put your cat inside and turn it on, it will die. Another study has shown that geomagnetic storms may be linked to clinical depression. Findings also suggest that bipolar-disorder patients moods improve immediately after they undergo a specific MRI procedure. This supports my thesis: There invisible structures and dynamics which influence our life, may they be natural or artificial. With this, I would like to investigate how we would change our behaviour if we would be able to experience the invisible.

![]({{site.baseurl}}/images/EMF.svg)


I want to sense electromagnetic frequencies. For that I am using a design from the [ Lab 3 The Laboratory for Experimental Computer Science at the Academy of Media Arts](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/) which will serve as my input. The PCB contains a LT5534 sensor chip, a variaty of components and antennas. The lengths of antennas determines which frequencies can be read. The antenna is a piece of wire which will be connected by a terminal block to the PCB to be able to exchange the antennas.

The output will be several vibration motors which will be connected to an arduino via Mosfets, which make it possible for me to trigger each motor individually. I have choosen vibration as the output of the organ because it translates the signals directly onto our skin. It will make it possible to sense our environment without having to look, read or touch something. After living with the organ it is possible to learn the "language", which we can understand as tacit knowledge. Vibration is already being used to translate the environment for the visually impaired. I have tried out Eccentric Rotating Mass vibration motor (ERM) as well as Linear Resonant Actuators as possible outputs. I have decided to use the LRA’s as they overall size and feeling of the vibration work well for my needs.

### Antenna wire loop lengths

- 2.4 GHz = 12.5 cm / Bluetooth, WLAN
- 1800MHz = 16 cm / E Netz GSM
- 900MHz = 33 cm / D Netz GSM
- 500 MHz = 60 cm / DVBT K24 Television
- 100 MHz = 150 cm / FM Broadcast

### BOM

- 1x Ardunio Uno
- 1x copper PCB
- 3x Mosfet IRF Z44N
- 3x LRA haptic driver motors
- 2x Resistor 10K
- 1x Resistor 47R
- 3x Capacitor 1nF = 1000 pF
- 1x Capacitor 100nf = 10000pF
- 1x LT5534
- Wires, shrinking cable
- Solder iron and solder

### Schematic

![]({{site.baseurl}}/images/Electronic_Schematic.svg)


### Design

First, my designs were inspired by geometry, using basic shapes like triangles and circles connected by straight lines as the geometry. But when I was trying out the shapes with the mockups made out of cardboard, I have recognized that it felt very alien-like on the body. I have asked several of my friends to try out the straight shaped mockup in comparison to the rounded one. The feedback was that the rounded one felt more "natural" and something they would feel comfortable with wearing on the body. The next step was to try out different materials placed on different parts of the body.  The materials are depended on what materials I can use to distribute the vibrations. Hard materials as metals with distribute the vibrations well. Soft materials like wood, fabric etc will absorb the vibration and are therefore not useful. But as the organ will be worn on the body I needed to find a compromise. Therefore I am considering a bioplastic for the structure in combination with metal wire for the vibration distribution.

Then I have used my phone in combination and separate on the body to find the right placement. The vibrations will translate the spacial placement and intensity of the electromagnetic frequencies. Despite my first thoughts, the placing of the organ on one side of the body made little to no sense.

The idea is that the spacial experience/ feeling will be translated on to the body, to slowly learn the "language" of the invisible. This resulted in placing the organ on the torso. The back seemed a logical next step, whereas the front would be constraining and not unisex. Many examples like a vibration vest for the visually impaired (20) show that the body can adapt relatively quickly to this new system of spacial experience.

The final inspiration by material plastic "analogue filter". Through bending the plastic the reflections of light will show a bent version of reality. Which is for me the perfect analogy for my project. I have made videos and photos of the plastic bending the reflection of reality, to take it as an inspiration for the shapes I am using for the geometry.

I have used Illustrator to try out different versions of my sketches and then imported it into Rhinoceros CAD to model a realistic version of my idea.

![]({{site.baseurl}}/images/Illustration.png)

![]({{site.baseurl}}/images/3Dmodel.png)

I have made two versions for a first physical insight into the concept design. One which is 3D printed and one which is vinyl cut with adhesive copper tape which is being used for flexibles PCB’s.

The schematic for the EMF detector, the 3D design and a Step by Step tutorial can be downloaded from my [Gitlab Repo](https://gitlab.com/JessicaGuy/sensingtheinvisible/tree/master)
